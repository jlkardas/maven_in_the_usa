package edu.lehigh.cse216.jlk221.backend;

// Import the Spark package, so that we can make use of the "get" function to 
// create an HTTP GET route
import com.google.gson.Gson;
import com.google.api.client.googleapis.auth.oauth2.*;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.gson.*;
import edu.lehigh.cse216.jlk221.backend.Database.*;
import java.io.FileReader;
import java.io.IOException;
import java.lang.InterruptedException;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.MemcachedClientBuilder;
import net.rubyeye.xmemcached.XMemcachedClientBuilder;
import net.rubyeye.xmemcached.auth.AuthInfo;
import net.rubyeye.xmemcached.command.BinaryCommandFactory;
import net.rubyeye.xmemcached.exception.MemcachedException;
import net.rubyeye.xmemcached.utils.AddrUtil;
import spark.Spark;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

// Import Google's JSON library
import com.google.gson.*;
import java.util.Map;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;


import com.google.api.client.googleapis.auth.oauth2.*;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.auth.oauth2.TokenResponseException;
import com.google.api.client.util.DateTime;
import com.google.api.client.auth.oauth2.Credential;

import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
// import com.google.api.services.calendar.model;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.http.javanet.NetHttpTransport;

import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.io.InputStreamReader;
import java.io.InputStream;

/**
 * For now, our app creates an HTTP server that can only get and add data.
 */
public class App {
    public static void main(String[] args) {
        final Gson gson = new Gson();
        Spark.port(getIntFromEnv("PORT", 4567));

        // Set up the location for serving static files. If the STATIC_LOCATION
        // environment variable is set, we will serve from it. Otherwise, serve
        // from "/web"
        String static_location_override = System.getenv("STATIC_LOCATION");
        if (static_location_override == null) {
            Spark.staticFileLocation("/web");
        } else {
            Spark.staticFiles.externalLocation(static_location_override);
        }

        Map<String, String> env = System.getenv();
        String db_url = env.get("DATABASE_URL");

        String cors_enabled = env.get("CORS_ENABLED");
        if (cors_enabled.equals("True")) {
            final String acceptCrossOriginRequestsFrom = "*";
            final String acceptedCrossOriginRoutes = "GET,PUT,POST,DELETE,OPTIONS";
            final String supportedRequestHeaders = "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin";
            enableCORS(acceptCrossOriginRequestsFrom, acceptedCrossOriginRoutes, supportedRequestHeaders);
        }

        final Database database = Database.getDatabase(db_url);

        /* Google Calendar API */
        Spark.post("/authcode", (request, response) -> {
            response.type("application/json");
            String authCode = request.body();
            try {
                GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(new NetHttpTransport(),
                        JacksonFactory.getDefaultInstance(), "https://oauth2.googleapis.com/token",
                        "1075363736727-sn066f4b9morlhpkjhajsu0rshhsjr9m.apps.googleusercontent.com",
                        "Yp1K7dlhw3HR4hqrsFyplSlJ", authCode, "https://mysterious-mountain-99747.herokuapp.com")
                                .execute();
                String accessToken = tokenResponse.getAccessToken();
                GoogleIdToken idToken = tokenResponse.parseIdToken();

                GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);

                // Initialize Calendar service with valid OAuth credentials
                Calendar service = new Calendar.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance(),
                        credential).setApplicationName("Maven In The USA").build();

                String pageToken = null;
                List<String> results = new ArrayList<String>();
                do {
                    Events events = service.events().list("primary").setPageToken(pageToken).execute();
                    List<Event> items = events.getItems();
                    for (Event event : items) {
                        // System.out.println(event.getSummary());
                        results.add(event.getSummary());
                    }
                    pageToken = events.getNextPageToken();
                } while (pageToken != null);
                return gson.toJson(results);
            } catch (TokenResponseException e) {
                return gson.toJson(e.getMessage());
            } catch (Exception e) {
                return gson.toJson("Exception occured: " + e.getMessage());
            }
        });

        

        // POST route for adding a new element to the Database. This will read
        // JSON from the body of the request, turn it into a SimpleRequest
        // object, extract the title and message, insert them, and return the
        // ID of the newly created row.
        Spark.post("/messages", (request, response) -> {
            // NB: if gson.Json fails, Spark will reply with status 500 Internal
            // Server Error
            SimpleRequest req = gson.fromJson(request.body(), SimpleRequest.class);
            // ensure status 200 OK, with a MIME type of JSON
            // NB: even on error, we return 200, but with a JSON object that
            // describes the error.
            response.status(200);
            response.type("application/json");
            // NB: createEntry checks for null title and message
            // System.out.print(req.mTitle + ", " + req.mMessage);
            int newId = database.insertRow(req.mTitle, req.mMessage, req.mEventId);
            if (newId == 0) {
                return gson.toJson(new StructuredResponse("error", "error performing insertion", null));
            } else {
                return gson.toJson(new StructuredResponse("ok", "" + newId, null));
            }
        });

        // deletes specific message wih id
        Spark.delete("/messages/:id", (request, response) -> {
            int idx = Integer.parseInt(request.params("id"));
            response.status(200);
            response.type("application/json");
            int result = database.deleteRow(idx);
            if (result == 0) {
                return gson.toJson(new StructuredResponse("error", "unable to delete row " + idx, null));
            } else {
                return gson.toJson(new StructuredResponse("ok", null, null));
            }
        });

        // GET route that returns all message titles and Ids. All we do is get
        // the data, embed it in a StructuredResponse, turn it into JSON, and
        // return it. If there's no data, we return "[]", so there's no need
        // for error handling.
        Spark.get("/messages", (request, response) -> {
            // ensure status 200 OK, with a MIME type of JSON
            response.status(200);
            response.type("application/json");
            return gson.toJson(new StructuredResponse("ok", null, database.selectAll()));
        });

        // GET route that returns everything for a single row in the Database.
        // The ":id" suffix in the first parameter to get() becomes
        // request.params("id"), so that we can get the requested row ID. If
        // ":id" isn't a number, Spark will reply with a status 500 Internal
        // Server Error. Otherwise, we have an integer, and the only possible
        // error is that it doesn't correspond to a row with data.
        Spark.get("/messages/:id", (request, response) -> {
            int idx = Integer.parseInt(request.params("id"));
            // ensure status 200 OK, with a MIME type of JSON
            response.status(200);
            response.type("application/json");
            RowData data = database.selectOne(idx);
            if (data == null) {
                return gson.toJson(new StructuredResponse("error", idx + " not found", null));
            } else {
                return gson.toJson(new StructuredResponse("ok", null, data));
            }
        });

        /*
        // Create specific message event with calendarId
        Spark.get("/messages/:eventId", (request, response) -> {
            int eventIdx = Integer.parseInt(request.params("eventId"));
            response.status(200);
            response.type("application/json");
            int result = database.selectEvent(calendarId, event);
            if (result == 0) {
                return gson.toJson(new StructuredResponse("error", "unable to update row " + calendarId, null));
            } else {
                return gson.toJson(new StructuredResponse("ok", null, result));
            }
        });

        // POST route for adding a new element to the Database. This will read
        // JSON from the body of the request, turn it into a SimpleRequest
        // object, extract the eventId, insert it, and return the
        // ID of the newly created row.
        Spark.post("/messages/:eventId", (request, response) -> {
            // NB: if gson.Json fails, Spark will reply with status 500 Internal
            // Server Error
            SimpleRequest req = gson.fromJson(request.body(), SimpleRequest.class);
            // ensure status 200 OK, with a MIME type of JSON
            // NB: even on error, we return 200, but with a JSON object that
            // describes the error.
            response.status(200);
            response.type("application/json");
            // NB: createEntry checks for null title and message
            // System.out.print(req.mTitle + ", " + req.mMessage);
            int newId = database.createEvent(req.mEventId);
            if (newId == 0) {
                return gson.toJson(new StructuredResponse("error", "error performing insertion", null));
            } else {
                return gson.toJson(new StructuredResponse("ok", "" + newId, null));
            }
        });

        // POST route for adding a new element to the Database. This will read
        // JSON from the body of the request, turn it into a SimpleRequest
        // object, extract the eventId, insert it, and return the
        // ID of the newly created row.
        Spark.post("/comments/:eventId", (request, response) -> {
            // NB: if gson.Json fails, Spark will reply with status 500 Internal
            // Server Error
            SimpleRequest req = gson.fromJson(request.body(), SimpleRequest.class);
            // ensure status 200 OK, with a MIME type of JSON
            // NB: even on error, we return 200, but with a JSON object that
            // describes the error.
            response.status(200);
            response.type("application/json");
            // NB: createEntry checks for null title and message
            // System.out.print(req.mTitle + ", " + req.mMessage);
            int newId = database.createEvent(req.mEventId);
            if (newId == 0) {
                return gson.toJson(new StructuredResponse("error", "error performing insertion", null));
            } else {
                return gson.toJson(new StructuredResponse("ok", "" + newId, null));
            }
        });
        */

        // updates specific message with id
        Spark.put("/messages/:id", (request, response) -> {
            int idx = Integer.parseInt(request.params("id"));
            SimpleRequest req = gson.fromJson(request.body(), SimpleRequest.class);
            response.status(200);
            response.type("application/json");
            int result = database.updateOne(idx, req.mTitle, req.mMessage);
            if (result == 0) {
                return gson.toJson(new StructuredResponse("error", "unable to update row " + idx, null));
            } else {
                return gson.toJson(new StructuredResponse("ok", null, result));
            }
        });

        // individual message upvotes
        Spark.put("/messages/:id/upvotes", (request, response) -> {
            int idx = Integer.parseInt(request.params("id"));
            SimpleRequest req = gson.fromJson(request.body(), SimpleRequest.class);
            response.status(200);
            response.type("application/json");
            int result = database.updateUpvotes(idx);
            if (result == 0) {
                return gson.toJson(new StructuredResponse("error", "unable to update row " + idx, null));
            } else {
                return gson.toJson(new StructuredResponse("ok", null, result));
            }
        });

        // individual message downvotes
        Spark.put("/messages/:id/downvotes", (request, response) -> {
            int idx = Integer.parseInt(request.params("id"));
            SimpleRequest req = gson.fromJson(request.body(), SimpleRequest.class);
            response.status(200);
            response.type("application/json");
            int result = database.updateDownvotes(idx);
            if (result == 0) {
                return gson.toJson(new StructuredResponse("error", "unable to update row " + idx, null));
            } else {
                return gson.toJson(new StructuredResponse("ok", null, result));
            }
        });

        // all users
        Spark.get("/users", (request, response) -> {
            // ensure status 200 OK, with a MIME type of JSON
            response.status(200);
            response.type("application/json");
            return gson.toJson(new StructuredResponse("ok", null, database.selectAllUsers()));
        });

        // get individual user id with given email
        Spark.get("/users/id/:email", (request, response) -> {
            String emailx = request.params("email");
            // ensure status 200 OK, with a MIME type of JSON
            response.status(200);
            response.type("application/json");
            RowData data = database.selectOneUId(emailx);
            if (data == null) {
                return gson.toJson(new StructuredResponse("error", emailx + " not found", null));
            } else {
                return gson.toJson(new StructuredResponse("ok", null, data));
            }
        });

        // get individual user email with given id
        Spark.get("/users/:id/email", (request, response) -> {
            int idx = Integer.parseInt(request.params("id"));
            // ensure status 200 OK, with a MIME type of JSON
            response.status(200);
            response.type("application/json");
            RowData data = database.selectOneEmail(idx);
            if (data == null) {
                return gson.toJson(new StructuredResponse("error", idx + " not found", null));
            } else {
                return gson.toJson(new StructuredResponse("ok", null, data));
            }
        });

        // ***EDIT THIS***
        // individual user comment
        Spark.post("/users/id/:comment", (request, response) -> {
            // NB: if gson.Json fails, Spark will reply with status 500 Internal
            // Server Error
            SimpleRequest req = gson.fromJson(request.body(), SimpleRequest.class);
            // ensure status 200 OK, with a MIME type of JSON
            // NB: even on error, we return 200, but with a JSON object that
            // describes the error.
            response.status(200);
            response.type("application/json");
            // NB: createEntry checks for null title and message
            // System.out.print(req.mTitle + ", " + req.mMessage);
            int newId = database.insertComment(req.mTitle, req.mMessage, req.mEventId);
            if (newId == 0) {
                return gson.toJson(new StructuredResponse("error", "error performing insertion", null));
            } else {
                return gson.toJson(new StructuredResponse("ok", "" + newId, null));
            }
        });

        // updates individual user password
        Spark.put("/users/password/:newPassword/:key", (request, response) -> {
            response.status(200);
            int key = Integer.parseInt(request.params("key"));
            String newPassword = request.params("newPassword");
            int res = database.updatePassword(newPassword, key);
            if (res == 1) {
                return -1;
            }
            return 1;
        });

        // validates login information
        Spark.put("/users/:email/:password", (request, response) -> {
            response.status(200);
            String email = request.params("email");
            String password = request.params("password");
            int key = database.validateLogin(password, email);
            if (key > 0) {
                return key;
            } else {
                return -1;
            }
        });

        // deletes key
        Spark.post("/usr/logout/:key", (request, response) -> {
            response.status(200);
            int key = Integer.parseInt(request.params("key"));
            boolean flag = database.deleteKey(key);
            return flag;
        });

    }
    public static MemcachedClient memcacheClientHelper() {
        List<InetSocketAddress> servers = AddrUtil.getAddresses(System.getenv("MEMCACHIER_SERVERS").replace(",", " "));
        AuthInfo authInfo = AuthInfo.plain(System.getenv("MEMCACHIER_USERNAME"), System.getenv("MEMCACHIER_PASSWORD"));

        MemcachedClientBuilder builder = new XMemcachedClientBuilder(servers);


        // Configure SASL auth for each server
        for (InetSocketAddress server : servers) {
            builder.addAuthInfo(server, authInfo);
        }

        // Use binary protocol
        builder.setCommandFactory(new BinaryCommandFactory());
        // Connection timeout in milliseconds (default: )
        builder.setConnectTimeout(1000);
        // Reconnect to servers (default: true)
        builder.setEnableHealSession(true);
        // Delay until reconnect attempt in milliseconds (default: 2000)
        builder.setHealSessionInterval(2000);

        try {            
            MemcachedClient mc = builder.build();
            try {
                mc.set("foo", 0, "bar");
                String val = mc.get("foo");
                System.out.println(val);
              } catch (TimeoutException te) {
                System.err.println("Timeout during set or get: " + te.getMessage());
              } catch (InterruptedException ie) {
                System.err.println("Interrupt during set or get: " + ie.getMessage());
              } catch (MemcachedException me) {
                System.err.println("Memcached error during get or set: " + me.getMessage());
              }
        } catch (IOException ioe) {
            System.err.println("Couldn't create a connection to MemCachier: " + ioe.getMessage()); 
        }
        return null;
    }

    

    /**
     * Get an integer environment varible if it exists, and otherwise return the
     * default value.
     * 
     * @envar The name of the environment variable to get.
     * @defaultVal The integer value to use as the default if envar isn't found
     * 
     * @returns The best answer we could come up with for a value for envar
     */
    static int getIntFromEnv(String envar, int defaultVal) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get(envar) != null) {
            return Integer.parseInt(processBuilder.environment().get(envar));
        }
        return defaultVal;
    }

    /**
     * Set up CORS headers for the OPTIONS verb, and for every response that the
     * server sends. This only needs to be called once.
     * 
     * @param origin  The server that is allowed to send requests to this server
     * @param methods The allowed HTTP verbs from the above origin
     * @param headers The headers that can be sent with a request from the above
     *                origin
     */
    private static void enableCORS(String origin, String methods, String headers) {
        // Create an OPTIONS route that reports the allowed CORS headers and methods
        Spark.options("/*", (request, response) -> {
            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }
            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }
            return "OK";
        });

        // 'before' is a decorator, which will run before any
        // get/post/put/delete. In our case, it will put three extra CORS
        // headers into the response
        Spark.before((request, response) -> {
            response.header("Access-Control-Allow-Origin", origin);
            response.header("Access-Control-Request-Method", methods);
            response.header("Access-Control-Allow-Headers", headers);
        });
    }

}