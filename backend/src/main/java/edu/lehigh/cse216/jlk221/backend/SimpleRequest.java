package edu.lehigh.cse216.jlk221.backend;

/**
 * SimpleRequest provides a format for clients to present title and message 
 * strings to the server.
 * 
 * NB: since this will be created from JSON, all fields must be public, and we
 *     do not need a constructor.
 */
public class SimpleRequest {
    /**
     * The title being provided by the client.
     */
    public String mTitle;

    /**
     * The message being provided by the client.
     */
    public String mMessage;

    /**
     * The clients id.
     */
    public int mId;

    /**
     * The number of upvotes the message has.
     */
    public int mUpvotes;

    /**
     * The number of downvotes the message has.
     */
    public int mDownvotes;

    /**
     * The event id.
     */
    public String mEventId;
}