package edu.lehigh.cse216.jlk221.backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.*;

import java.net.URI;
import java.net.URISyntaxException;

public class Database {
    /**
     * The connection to the database. When there is no connection, it should be
     * null. Otherwise, there is a valid open connection
     */
    private Connection mConnection;

    /**
     * A prepared statement for inserting into the database
     */
    private PreparedStatement mInsertOne;

    /**
     * A prepared statement for deleting a row from the database
     */
    private PreparedStatement mDeleteOne;

    /**
     * A prepared statement for getting all data in the database
     */
    private PreparedStatement mSelectAll;

    /**
     * A prepared statement for getting one row from the database
     */
    private PreparedStatement mSelectOne;

    /**
     * A prepared statement for updating a single row in the database
     */
    private PreparedStatement mUpdateOne;

    /**
     * A prepared statement for creating the table in our database
     *
    private PreparedStatement mCreateTable;

    /**
     * A prepared statement for dropping the table in our database
     *
    private PreparedStatement mDropTable;
    */

    /**
     * A prepared statement for updating a message's upvotes
     */
    private PreparedStatement mUpdateUpvotes;

    /**
     * A prepared statement for updating a message's downvotes
     */
    private PreparedStatement mUpdateDownvotes;

    /**
     * A prepared statement for getting all data in the database
     */
    private PreparedStatement mSelectAllUsers;

    /**
     * A prepared statement for getting one email
     */
    private PreparedStatement mSelectOneEmail;

    /**
     * A prepared statement for getting one email
     */
    private PreparedStatement mSelectOneUId;

    /**
     * Prepared statements for handling passwords
     */
    private PreparedStatement mGetSalt;

    private PreparedStatement mUpdatePassword;

    private PreparedStatement mGetPassword;

    private PreparedStatement mGetUserId;

    /**
     * A prepared statement for inserting a comment
     */
    private PreparedStatement mInsertComment;

    private HashMap <Integer, Integer> sessionKeyTbl;
    private String password;

    /**
     * RowData is like a struct in C: we use it to hold data, and we allow direct
     * access to its fields. In the context of this Database, RowData represents the
     * data we'd see in a row.
     * 
     * We make RowData a static class of Database because we don't really want to
     * encourage users to think of RowData as being anything other than an abstract
     * representation of a row of the database. RowData and the Database are tightly
     * coupled: if one changes, the other should too.
     */
    public static class RowData {
        /**
         * The ID of this row of the database
         */
        int mId;
        /**
         * The subject stored in this row
         */
        String mTitle;
        /**
         * The message stored in this row
         */
        String mMessage;
        /**
         * The number of upvotes stored in this row
         */
        int mUpvotes;
        /**
         * The number of upvotes stored in this row
         */
        int mDownvotes;
        /**
         * The user stored in this row
         */
        String mUser;
        /**
         * The email stored in this row
         */
        String mEmail;        

        int mData;
        int[] mComments[];
        String mEventId;

        /**
         * Construct a RowData object by providing values for its fields
         */
        public RowData(int id, String subject, String message, String user, String email, String eventId) {
            mId = id;
            mTitle = subject;
            mMessage = message;
            mUpvotes = 0;
            mDownvotes = 0;
            mUser = user;
            mEmail = email;
            mEventId = eventId;
        }
    }

    /**
     * The Database constructor is private: we only create Database objects through
     * the getDatabase() method.
     */
    private Database() {
    }
    
    /**
     * Get a fully-configured connection to the database
     * 
     * @param ip   The IP address of the database server
     * @param port The port on the database server to which connection requests
     *             should be sent
     * @param user The user ID to use when connecting
     * @param pass The password to use when connecting
     * 
     * @return A Database object, or null if we cannot connect properly
     */
    static Database getDatabase(String db_url) {
        // Create an un-configured Database object
        Database db = new Database();

        Map<String, String> env = System.getenv();
        //String db_url = env.get("DATABASE_URL");

        // Give the Database object a connection, fail if we cannot get one
        try {
            Class.forName("org.postgresql.Driver");
            URI dbUri = new URI(db_url);
            String username = dbUri.getUserInfo().split(":")[0];
            String password = dbUri.getUserInfo().split(":")[1];
            String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();
            Connection conn = DriverManager.getConnection(dbUrl, username, password);
            if (conn == null) {
                System.err.println("Error: DriverManager.getConnection() returned a null object");
                return null;
            }
            db.mConnection = conn;
        } catch (SQLException e) {
            System.err.println("Error: DriverManager.getConnection() threw a SQLException");
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException cnfe) {
            System.out.println("Unable to find postgresql driver");
            return null;
        } catch (URISyntaxException s) {
            System.out.println("URI Syntax Error");
            return null;
        }

        // Attempt to create all of our prepared statements. If any of these
        // fail, the whole getDatabase() call should fail
        try {
            // NB: we can easily get ourselves in trouble here by typing the
            // SQL incorrectly. We really should have things like "tblData"
            // as constants, and then build the strings for the statements
            // from those constants.

            // Note: no "IF NOT EXISTS" or "IF EXISTS" checks on table
            // creation/deletion, so multiple executions will cause an exception
            /*
            db.mCreateTable = db.mConnection
                    .prepareStatement("CREATE TABLE Messages (id SERIAL PRIMARY KEY, subject VARCHAR(50) "
                            + "NOT NULL, message VARCHAR(500) NOT NULL)");
            db.mDropTable = db.mConnection.prepareStatement("DROP TABLE Messages");
            */

            // CRUD operations
            db.mInsertOne = db.mConnection.prepareStatement("INSERT INTO Messages VALUES (default, ?, ?, ?)");
            db.mDeleteOne = db.mConnection.prepareStatement("DELETE FROM Messages WHERE id = ?");
            db.mSelectAll = db.mConnection.prepareStatement("SELECT id, subject FROM Messages");
            db.mSelectOne = db.mConnection.prepareStatement("SELECT * from Messages WHERE id=?"); //db.mUpdateOne = db.mConnection.prepareStatement("UPDATE tblData SET message = ? WHERE id = ?");
            db.mUpdateOne = db.mConnection.prepareStatement("UPDATE Messages SET subject = ?, message = ? WHERE id = ?");
            db.mUpdateUpvotes = db.mConnection.prepareStatement("UPDATE Messages SET upvotes = ? WHERE id = ?");
            db.mUpdateDownvotes = db.mConnection.prepareStatement("UPDATE Messages SET downvotes = ? WHERE id = ?");            
            db.mSelectAllUsers = db.mConnection.prepareStatement("SELECT userId FROM Users");
            db.mSelectOneUId = db.mConnection.prepareStatement("SELECT userId from Users WHERE email = ?");
            db.mSelectOneEmail = db.mConnection.prepareStatement("SELECT email from Users WHERE userId = ?");
            db.mInsertComment = db.mConnection.prepareStatement("INSERT INTO Users VALUES (default, ?, ?)");
            db.mGetSalt = db.mConnection.prepareStatement("SELECT salt FROM Users WHERE userId = ?");
            db.mUpdatePassword = db.mConnection.prepareStatement("UPDATE Users SET password = ? WHERE userId = ?");
            db.mGetPassword = db.mConnection.prepareStatement("SELECT password FROM Users WHERE userId = ?");
            db.mGetUserId = db.mConnection.prepareStatement("SELECT userId FROM Users WHERE email = ?");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
            db.disconnect();
            return null;
        }
        return db;
    }

    /**
     * Close the current connection to the database, if one exists.
     * 
     * NB: The connection will always be null after this call, even if an error
     * occurred during the closing operation.
     * 
     * @return True if the connection was cleanly closed, false otherwise
     */
    boolean disconnect() {
        if (mConnection == null) {
            System.err.println("Unable to close connection: Connection was null");
            return false;
        }
        try {
            mConnection.close();
        } catch (SQLException e) {
            System.err.println("Error: Connection.close() threw a SQLException");
            e.printStackTrace();
            mConnection = null;
            return false;
        }
        mConnection = null;
        return true;
    }


    /**
     * Insert a row into the database
     * 
     * @param subject The subject for this new row
     * @param message The message body for this new row
     * 
     * @return The number of rows that were inserted
     */
    int insertRow(String subject, String message, String eventId) {
        int count = 0;
        try {
            mInsertOne.setString(1, subject);
            mInsertOne.setString(2, message);
            mInsertOne.setString(3, eventId);
            count += mInsertOne.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Delete a row by ID
     * 
     * @param id The id of the row to delete
     * 
     * @return The number of rows that were deleted. -1 indicates an error.
     */
    int deleteRow(int id) {
        int res = -1;
        try {
            mDeleteOne.setInt(1, id);
            res = mDeleteOne.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Query the database for a list of all subjects and their IDs
     * 
     * @return All rows, as an ArrayList
     */
    ArrayList<RowData> selectAll() {
        ArrayList<RowData> res = new ArrayList<RowData>();
        try {
            ResultSet rs = mSelectAll.executeQuery();
            while (rs.next()) {
                res.add(new RowData(rs.getInt("id"), null, rs.getString("subject"), null, null, null));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get all data for a specific row, by ID
     * 
     * @param id The id of the row being requested
     * 
     * @return The data for the requested row, or null if the ID was invalid
     */
    RowData selectOne(int id) {
        RowData res = null;
        try {
            mSelectOne.setInt(1, id);
            ResultSet rs = mSelectOne.executeQuery();
            if (rs.next()) {
                res = new RowData(rs.getInt("id"), rs.getString("message"), rs.getString("subject"), null, null, null);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Update the message for a row in the database
     * 
     * @param id      The id of the row to update
     * @param message The new message contents
     * 
     * @return The number of rows that were updated. -1 indicates an error.
     */
    int updateOne(int id, String subject, String message) {
        int res = -1;
        try {
            mUpdateOne.setString(1, subject);
            mUpdateOne.setString(2, message);
            mUpdateOne.setInt(3, id);
            res = mUpdateOne.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Create tblData. If it already exists, this will print an error
     *
    void createTable() {
        try {
            mCreateTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove tblData from the database. If it does not exist, this will print an
     * error.
     *
    void dropTable() {
        try {
            mDropTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    */

    /**
     * Update the upvotes for a row in the database
     * 
     * @param id      The id of the row to update
     * @param upvotes The new message upvotes
     * 
     * @return The number of rows that were updated. -1 indicates an error.
     */
    int updateUpvotes(int id) {
        int res = -1;
        try {
            mUpdateUpvotes.setInt(1, id);
            res = mUpdateUpvotes.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Update the downvotes for a row in the database
     * 
     * @param id      The id of the row to update
     * @param downvotes The new message downvotes
     * 
     * @return The number of rows that were updated. -1 indicates an error.
     */
    int updateDownvotes(int id) {
        int res = -1;
        try {
            mUpdateDownvotes.setInt(1, id);
            res = mUpdateDownvotes.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Query the database for a list of all users and their IDs
     * 
     * @return All rows, as an ArrayList
     */
    ArrayList<RowData> selectAllUsers() {
        ArrayList<RowData> res = new ArrayList<RowData>();
        try {
            ResultSet rs = mSelectAllUsers.executeQuery();
            while (rs.next()) {
                res.add(new RowData(rs.getInt("id"), null, null, null, null, null));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get all data for a specific row, by ID
     * 
     * @param id The id of the row being requested
     * 
     * @return The data for the requested row, or null if the ID was invalid
     */
    RowData selectOneUId(String email) {
        RowData res = null;
        try {
            mSelectOne.setString(1, email);
            ResultSet rs = mSelectOneUId.executeQuery();
            if (rs.next()) {
                res = new RowData(rs.getInt("id"), null, null, null, rs.getString("email"), null);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Get all data for a specific row, by ID
     * 
     * @param id The id of the row being requested
     * 
     * @return The data for the requested row, or null if the ID was invalid
     */
    RowData selectOneEmail(int id) {
        RowData res = null;
        try {
            mSelectOne.setInt(1, id);
            ResultSet rs = mSelectOneEmail.executeQuery();
            if (rs.next()) {
                res = new RowData(rs.getInt("id"), null, null, null, rs.getString("email"), null);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Insert a row into the database
     * 
     * CHANGE SUBJECT AND MESSAGE
     * @param subject The subject for this new row
     * @param message The message body for this new row
     * 
     * @return The number of rows that were inserted
     */
    int insertComment(String subject, String message, String eventId) {
        int count = 0;
        try {
            mInsertOne.setString(1, subject);
            mInsertOne.setString(2, message);
            mInsertOne.setString(3, eventId);
            count += mInsertComment.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    int createSessionKey(int userId) {
        if (sessionKeyTbl.size() == 0) {
            sessionKeyTbl.put(userId, 1);
        }
        else {
            try {
                Enumeration<Integer> enumeration = (Enumeration<Integer>) sessionKeyTbl.values();
                int highVal = -1;
                while(enumeration.hasMoreElements()){
                    int temp = enumeration.nextElement();
                    if(highVal < temp){
                        highVal = temp;
                    }
                }
                highVal++;
                sessionKeyTbl.put(userId, highVal);
                return highVal;
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    boolean checkKey(int userId, int key) {
        boolean flag = false;
        try {
            Iterator<Map.Entry<Integer, Integer> > 
            iterator = sessionKeyTbl.entrySet().iterator(); 
            Map.Entry<Integer, Integer> entry = iterator.next(); 
            if (key == entry.getValue()) { 
                flag = true;
                return flag; 
            } 
        }
        catch(Exception e) {
            return flag;
        }
        return false;
    }

    boolean loginCheck(int userId) {
        boolean flag = false;
        try {
            Iterator<Map.Entry<Integer, Integer> > 
            iterator = sessionKeyTbl.entrySet().iterator(); 
            Map.Entry<Integer, Integer> entry = iterator.next(); 
            if (userId == entry.getKey()) { 
                flag = true;
                return flag; 
            } 
        }
        catch(Exception e) {
            return flag;
        }
        return false;
    }

    int getUserId(int key) {
        try {
            Iterator<Map.Entry<Integer, Integer> > 
                iterator = sessionKeyTbl.entrySet().iterator(); 
                Map.Entry<Integer, Integer> entry = iterator.next(); 
                if (key == entry.getValue()) { 
                    return entry.getKey();
                } 
        }
        catch(Exception e) {
            return -1;
        }
        return -1;
    }

    boolean deleteKey(int key) {
        int userId = getUserId(key);
        sessionKeyTbl.remove(userId);
        boolean flag = loginCheck(userId);
        if(flag) {
            return true;
        }
        else{
            return false;
        }
    }

    int updatePassword(String password, int key){
        int userId = getUserId(key);
        boolean flag = checkKey(userId, key);
        if(flag){
            int res = -1;
            try{
                mUpdatePassword.setInt(2, userId);
                mGetSalt.setInt(1, userId);
                byte[] salt = mGetSalt.executeQuery().getBytes("salt");
                byte[] newPassword = PasswordSecurity.generateStorngPasswordHash(password, salt).getBytes();
                mUpdatePassword.setBytes(1,newPassword);
                res = mUpdatePassword.executeUpdate();
                return res;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return -1;
        }
        return -1;
    }
    int validateLogin(String password, String email){
        boolean res = false;
        try{
            mGetUserId.setString(1, email);
            int userId = mGetUserId.executeQuery().getInt("UserId");

            mGetPassword.setInt(1, userId);
            byte[] storedPassword = mGetPassword.executeQuery().getBytes("password");

            String sStoredPassword = storedPassword.toString();
            res = PasswordSecurity.validatePassword(password, sStoredPassword);
            boolean loginCheck = loginCheck(userId);
            if(loginCheck == false && res == true){
                    int key = createSessionKey(userId);
                    return key;
            }
        } 
        catch(Exception e){
            e.printStackTrace();
        }
        return -1;
    }
}