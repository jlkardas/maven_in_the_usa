"use strict";
var EditEntryForm = /** @class */ (function () {
    function EditEntryForm() {
    }
    EditEntryForm.init = function () {
        if (!EditEntryForm.isInit) {
            $("body").append(Handlebars.templates[EditEntryForm.NAME + ".hb"]());
            $("#" + EditEntryForm.NAME + "-OK").click(EditEntryForm.submitForm);
            $("#" + EditEntryForm.NAME + "-Close").click(EditEntryForm.hide);
            EditEntryForm.isInit = true;
        }
    };
    EditEntryForm.refresh = function () {
        EditEntryForm.init();
    };
    EditEntryForm.hide = function () {
        $("#" + EditEntryForm.NAME + "-title").val("");
        $("#" + EditEntryForm.NAME + "-message").val("");
        $("#" + EditEntryForm.NAME).modal("hide");
    };
    EditEntryForm.show = function (id) {
        EditEntryForm.id = id;
        $("#" + EditEntryForm.NAME + "-title").val("");
        $("#" + EditEntryForm.NAME + "-message").val("");
        $("#" + EditEntryForm.NAME).modal("show");
    };
    EditEntryForm.clearForm = function () {
        $("#editTitle").val("");
        $("#editMessage").val("");
        $("#editId").val("");
        $("#editCreated").text("");
        // reset the UI
        $("#addElement").hide();
        $("#editElement").hide();
        $("#showElements").show();
    };
    EditEntryForm.submitForm = function () {
        // get the values of the two fields, force them to be strings, and check
        // that neither is empty
        var title = "" + $("#" + EditEntryForm.NAME + "-title").val();
        var msg = "" + $("#" + EditEntryForm.NAME + "-message").val();
        // NB: we assume that the user didn't modify the value of #editId
        if (title === "" || msg === "") {
            window.alert("Error: title or message is not valid");
            return;
        }
        // set up an AJAX post.  When the server replies, the result will go to
        // onSubmitResponse
        $.ajax({
            type: "PUT",
            url: "/messages/" + EditEntryForm.id,
            dataType: "json",
            data: JSON.stringify({ mTitle: title, mMessage: msg }),
            success: function (response) {
                EditEntryForm.onSubmitResponse(response);
            }
        });
    };
    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a
     * result.
     *
     * @param data The object returned by the server
     */
    EditEntryForm.onSubmitResponse = function (data) {
        // If we get an "ok" message, clear the form and refresh the main
        // listing of messages
        if (data.mStatus === "ok") {
            EditEntryForm.clearForm();
            EditEntryForm.hide();
            ElementList.refresh();
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    };
    EditEntryForm.NAME = "EditEntryForm";
    EditEntryForm.isInit = false;
    EditEntryForm.id = NaN;
    return EditEntryForm;
}());
/**
 * NewEntryForm encapsulates all of the code for the form for adding an entry
 */
var NewEntryForm = /** @class */ (function () {
    function NewEntryForm() {
    }
    /**
     * Initialize the NewEntryForm by creating its element in the DOM and
     * configuring its buttons.  This needs to be called from any public static
     * method, to ensure that the Singleton is initialized before use
     */
    NewEntryForm.init = function () {
        if (!NewEntryForm.isInit) {
            $("body").append(Handlebars.templates[NewEntryForm.NAME + ".hb"]());
            $("#" + NewEntryForm.NAME + "-OK").click(NewEntryForm.submitForm);
            $("#" + NewEntryForm.NAME + "-Close").click(NewEntryForm.hide);
            NewEntryForm.isInit = true;
        }
    };
    /**
     * Refresh() doesn't really have much meaning, but just like in sNavbar, we
     * have a refresh() method so that we don't have front-end code calling
     * init().
     */
    NewEntryForm.refresh = function () {
        NewEntryForm.init();
    };
    /**
     * Hide the NewEntryForm.  Be sure to clear its fields first
     */
    NewEntryForm.hide = function () {
        $("#" + NewEntryForm.NAME + "-title").val("");
        $("#" + NewEntryForm.NAME + "-message").val("");
        $("#" + NewEntryForm.NAME).modal("hide");
    };
    /**
     * Show the NewEntryForm.  Be sure to clear its fields, because there are
     * ways of making a Bootstrap modal disapper without clicking Close, and
     * we haven't set up the hooks to clear the fields on the events associated
     * with those ways of making the modal disappear.
     */
    NewEntryForm.show = function () {
        $("#" + NewEntryForm.NAME + "-title").val("");
        $("#" + NewEntryForm.NAME + "-message").val("");
        $("#" + NewEntryForm.NAME).modal("show");
    };
    /**
     * Send data to submit the form only if the fields are both valid.
     * Immediately hide the form when we send data, so that the user knows that
     * their click was received.
     */
    NewEntryForm.submitForm = function () {
        // get the values of the two fields, force them to be strings, and check
        // that neither is empty
        var title = "" + $("#" + NewEntryForm.NAME + "-title").val();
        var msg = "" + $("#" + NewEntryForm.NAME + "-message").val();
        if (title === "" || msg === "") {
            window.alert("Error: title or message is not valid");
            return;
        }
        NewEntryForm.hide();
        // set up an AJAX post.  When the server replies, the result will go to
        // onSubmitResponse
        $.ajax({
            type: "POST",
            url: "/messages",
            dataType: "json",
            data: JSON.stringify({ mTitle: title, mMessage: msg }),
            success: NewEntryForm.onSubmitResponse
        });
    };
    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a
     * result.
     *
     * @param data The object returned by the server
     */
    NewEntryForm.onSubmitResponse = function (data) {
        // If we get an "ok" message, clear the form and refresh the main
        // listing of messages
        if (data.mStatus === "ok") {
            ElementList.refresh();
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    };
    /**
     * The name of the DOM entry associated with NewEntryForm
     */
    NewEntryForm.NAME = "NewEntryForm";
    /**
     * Track if the Singleton has been initialized
     */
    NewEntryForm.isInit = false;
    return NewEntryForm;
}());
var ElementList = /** @class */ (function () {
    function ElementList() {
    }
    ElementList.init = function () {
        if (!ElementList.isInit) {
            ElementList.isInit = true;
        }
    };
    // Refresh method to initialize the elementList and get the messages from the server
    ElementList.refresh = function () {
        // Make sure the singleton is initialized
        ElementList.init();
        // Issue a GET, and then pass the result to update()
        $.ajax({
            type: "GET",
            url: "/messages",
            dataType: "json",
            success: function (response) {
                console.log(response);
                ElementList.update(response);
            }
        });
    };
    ElementList.update = function (data) {
        // Remove the table of data, if it exists
        $("#" + ElementList.NAME).remove();
        // Use a template to re-generate the table, and then insert it
        $("body").append(Handlebars.templates[ElementList.NAME + ".hb"](data));
        // Find all of the Edit buttons, and set their behavior
        $("." + ElementList.NAME + "-editbtn").click(ElementList.clickEdit);
        // Find all of the delete buttons, and set their behavior
        $("." + ElementList.NAME + "-delbtn").click(ElementList.clickDelete);
        // Find all of the upvote buttons, and set their behavior
        $("." + ElementList.NAME + "-upbtn").click(ElementList.clickUpvote);
        // Find all of the downvote buttons, and set their behavior
        $("." + ElementList.NAME + "-downbtn").click(ElementList.clickDownvote);
        // Find all of the downvote buttons, and set their behavior
        $("." + ElementList.NAME + "-cmtbtn").click(ElementList.sendComment);
        // Find all of the comments
        $("." + ElementList.NAME + "-actvbtn").click(ElementList.activity);
    };
    // Delete button handler
    ElementList.clickDelete = function () {
        var id = $(this).data("value");
        $.ajax({
            type: "DELETE",
            url: "/messages/" + id,
            dataType: "json",
            // TODO: we should really have a function that looks at the return
            //       value and possibly prints an error message.
            success: ElementList.refresh
        });
    };
    // Edit button handler
    ElementList.clickEdit = function () {
        var id = $(this).data("value");
        $.ajax({
            type: "GET",
            url: "/messages/" + id,
            dataType: "json",
            success: EditEntryForm.show(id)
        });
    };
    // Upvote button handler
    ElementList.clickUpvote = function () {
        var id = $(this).data("value");
        $.ajax({
            type: "PUT",
            url: "/messages/" + id + "/upvotes",
            dataType: "json",
            success: ElementList.refresh
        });
    };
    // Downvote button handler
    ElementList.clickDownvote = function () {
        var id = $(this).data("value");
        $.ajax({
            type: "PUT",
            url: "/messages/" + id + "/downvotes",
            dataType: "json",
            success: ElementList.refresh
        });
    };
    /*
      sendComment is called when the user clicks on the "Comment" button
    */
    ElementList.sendComment = function () {
        var id = $(this).data("value");
        console.log("PUT comment for id " + id + " : " + $(".commentBox").val());
        $.ajax({
            type: "PUT",
            url: "/messages/" + id + "/comments",
            dataType: "json",
            data: { comment: $(".commentBox").val() },
            success: function (response) {
                ElementList.refresh;
            }
        });
    };
    /*
      activity is called when the user clicks on the "Activity" button
    */
    ElementList.activity = function () {
        var id = $(this).data("value");
        $.ajax({
            type: "GET",
            url: "/messages/" + id + "/comments",
            dataType: "json",
            success: Activity.show(id)
        });
    };
    ElementList.NAME = "ElementList";
    ElementList.isInit = false;
    return ElementList;
}());
/**
 * The Navbar Singleton is the navigation bar at the top of the page.  Through
 * its HTML, it is designed so that clicking the "brand" part will refresh the
 * page.  Apart from that, it has an "add" button, which forwards to
 * NewEntryForm
 */
var Navbar = /** @class */ (function () {
    function Navbar() {
    }
    /**
     * Initialize the Navbar Singleton by creating its element in the DOM and
     * configuring its button.  This needs to be called from any public static
     * method, to ensure that the Singleton is initialized before use.
     */
    Navbar.init = function () {
        if (!Navbar.isInit) {
            $("body").prepend(Handlebars.templates[Navbar.NAME + ".hb"]());
            $("#" + Navbar.NAME + "-add").click(NewEntryForm.show);
            $("#" + Navbar.NAME + "-calendar").click(CalendarEvents.show);
            $("#" + Navbar.NAME + "-login").click(Login.show);
            $("#" + Navbar.NAME + "-profile").click(Profile.show);
            Navbar.isInit = true;
        }
    };
    /**
     * Refresh() doesn't really have much meaning for the navbar, but we'd
     * rather not have anyone call init(), so we'll have this as a stub that
     * can be called during front-end initialization to ensure the navbar
     * is configured.
     */
    Navbar.refresh = function () {
        Navbar.init();
    };
    /**
     * Track if the Singleton has been initialized
     */
    Navbar.isInit = false;
    /**
     * The name of the DOM entry associated with Navbar
     */
    Navbar.NAME = "Navbar";
    return Navbar;
}());
var Login = /** @class */ (function () {
    function Login() {
    }
    Login.init = function () {
        if (!Login.isInit) {
            $("body").append(Handlebars.templates[Login.NAME + ".hb"]());
            $("#" + Login.NAME + "-Login").click(Login.submitLogin);
            $("#" + Login.NAME + "-Exit").click(Login.hide);
            Login.isInit = true;
        }
    };
    Login.refresh = function () {
        Login.init();
    };
    Login.hide = function () {
        $("#" + Login.NAME + "-username").val("");
        $("#" + Login.NAME + "-password").val("");
        $("#" + Login.NAME).modal("hide");
    };
    Login.show = function () {
        $("#" + Login.NAME + "-username").val("");
        $("#" + Login.NAME + "-password").val("");
        $("#" + Login.NAME + "-Success").hide();
        $("#" + Login.NAME).modal("show");
    };
    // frontend sends backend get request with email of user loging in
    Login.submitLogin = function () {
        var e = $("#" + Login.NAME + "-email").val();
        var p = $("#" + Login.NAME + "-password").val();
        //console.log("Email: " + u);
        //console.log("Password: " + p);
        if (e === "" || p === "") {
            window.alert("Error: title or message is not valid");
            return;
        }
        Login.hide();
        $.ajax({
            type: "PUT",
            url: /*backendURL +*/ "/users/" + e,
            dataType: "json",
            success: function (response) {
                console.log(response);
                // TODO: if the resposne is good, update the Profile to show
                // any releveant information associated with the user
                $("#Profile-username").val("");
                $("#Profile-email").val("");
                $("#Profile-password").val("");
                $("#Profile-comment").val("");
            }
        });
        /* PREVIOUS CODE
        $.ajax({
          type: "GET",
          url: "/users/" + e,
          dataType: "json",
          success: (response: JSON) => {
            console.log(response);
            // TODO: if the resposne is good, update the Profile to show
            // any releveant information associated with the user
            $("#Profile-username").val("");
            $("#Profile-email").val("");
            $("#Profile-password").val("");
            $("#Profile-comment").val("");
            Login.hide();
          }
        });
        */
    };
    Login.NAME = "Login";
    Login.isInit = false;
    return Login;
}());
var Profile = /** @class */ (function () {
    function Profile() {
    }
    Profile.init = function () {
        if (!Profile.isInit) {
            $("body").append(Handlebars.templates[Profile.NAME + ".hb"]());
            //   $("#" + Profile.NAME + "-Profile").click(Profile.submitProfile);
            $("#" + Profile.NAME + "-Close").click(Profile.hide);
            Profile.isInit = true;
        }
    };
    Profile.refresh = function () {
        Profile.init();
    };
    Profile.hide = function () {
        $("#" + Profile.NAME + "-username").val("");
        $("#" + Profile.NAME + "-email").val("");
        $("#" + Profile.NAME + "-password").val("");
        $("#" + Profile.NAME + "-comment").val("");
        $("#" + Profile.NAME).modal("hide");
    };
    Profile.show = function () {
        $("#" + Profile.NAME + "-username").val("");
        $("#" + Profile.NAME + "-email").val("");
        $("#" + Profile.NAME + "-password").val("");
        $("#" + Profile.NAME + "-edit-password").val("");
        $("#" + Profile.NAME + "-comment").val("");
        $("#" + Profile.NAME + "-edit-comment").val("");
        $("#" + Profile.NAME + "-Success").hide();
        $("#" + Profile.NAME).modal("show");
    };
    Profile.NAME = "Profile";
    Profile.isInit = false;
    return Profile;
}());
var Activity = /** @class */ (function () {
    function Activity() {
    }
    Activity.init = function () {
        if (!Activity.isInit) {
            $("body").append(Handlebars.templates[Activity.NAME + ".hb"]());
            $("#" + Activity.NAME + "-OK").click(Activity.submitForm);
            $("#" + Activity.NAME + "-Close").click(Activity.hide);
            Activity.isInit = true;
        }
    };
    Activity.refresh = function () {
        Activity.init();
    };
    Activity.hide = function () {
        $("#" + Activity.NAME + "-title").val("");
        $("#" + Activity.NAME + "-message").val("");
        $("#" + Activity.NAME + "-comment").val("");
        $("#" + Activity.NAME).modal("hide");
    };
    Activity.show = function (id) {
        Activity.id = id;
        $("#" + Activity.NAME + "-title").val("");
        $("#" + Activity.NAME + "-message").val("");
        $("#" + Activity.NAME + "-comment").val("");
        $("#" + Activity.NAME).modal("show");
    };
    Activity.submitForm = function () {
        // get the values of the two fields, force them to be strings, and check
        // that neither is empty
        var title = "" + $("#" + Activity.NAME + "-title").val();
        var msg = "" + $("#" + Activity.NAME + "-message").val();
        var comment = "" + $("#" + Activity.NAME + "-comment").val();
        // NB: we assume that the user didn't modify the value of #editId
        if (title === "" || msg === "" || comment === "") {
            window.alert("Error: title or message is not valid");
            return;
        }
        // set up an AJAX post.  When the server replies, the result will go to
        // onSubmitResponse
        $.ajax({
            type: "POST",
            url: "/messages/" + Activity.id + "/comment/",
            dataType: "json",
            data: JSON.stringify({ mTitle: title, mMessage: msg, mComment: comment }),
            success: function (response) {
                Activity.onSubmitResponse(response);
            }
        });
    };
    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a
     * result.
     *
     * @param data The object returned by the server
     */
    Activity.onSubmitResponse = function (data) {
        // If we get an "ok" message, clear the form and refresh the main
        // listing of messages
        if (data.mStatus === "ok") {
            Activity.hide();
            Activity.refresh();
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    };
    Activity.NAME = "Activity";
    Activity.isInit = false;
    Activity.id = NaN;
    return Activity;
}());
// import { google } from "googleapis";
// const calendar = google.calendar_v3;
// const SCOPES = ["https://www.googleapis.com/auth/calendar.readonly"];
/**
 * CalendarEvents encapsulates all of the code for the form for adding an entry
 */
var CalendarEvents = /** @class */ (function () {
    function CalendarEvents() {
    }
    /**
     * Initialize the CalendarEvents by creating its element in the DOM and
     * configuring its buttons.  This needs to be called from any public static
     * method, to ensure that the Singleton is initialized before use
     */
    CalendarEvents.init = function () {
        if (!CalendarEvents.isInit) {
            $("body").append(Handlebars.templates[CalendarEvents.NAME + ".hb"]());
            $("#" + CalendarEvents.NAME + "-Close").click(CalendarEvents.hide);
            CalendarEvents.isInit = true;
        }
    };
    /**
     * Refresh() doesn't really have much meaning, but just like in sNavbar, we
     * have a refresh() method so that we don't have front-end code calling
     * init().
     */
    CalendarEvents.refresh = function () {
        CalendarEvents.init();
    };
    /**
     * Hide the CalendarEvents.  Be sure to clear its fields first
     */
    CalendarEvents.hide = function () {
        // $("#" + CalendarEvents.NAME + "-title").val("");
        // $("#" + CalendarEvents.NAME + "-message").val("");
        $("#" + CalendarEvents.NAME).modal("hide");
    };
    /**
     * Show the CalendarEvents.  Be sure to clear its fields, because there are
     * ways of making a Bootstrap modal disapper without clicking Close, and
     * we haven't set up the hooks to clear the fields on the events associated
     * with those ways of making the modal disappear.
     */
    CalendarEvents.show = function () {
        // $("#" + CalendarEvents.NAME + "-title").val("");
        // $("#" + CalendarEvents.NAME + "-message").val("");
        $("#" + CalendarEvents.NAME).modal("show");
    };
    /**
     * Send data to submit the form only if the fields are both valid.
     * Immediately hide the form when we send data, so that the user knows that
     * their click was received.
     */
    CalendarEvents.submitForm = function () {
        // get the values of the two fields, force them to be strings, and check
        // that neither is empty
        // let title = "" + $("#" + CalendarEvents.NAME + "-title").val();
        // let msg = "" + $("#" + CalendarEvents.NAME + "-message").val();
        // if (title === "" || msg === "") {
        //   window.alert("Error: title or message is not valid");
        //   return;
        // }
        // CalendarEvents.hide();
        // // set up an AJAX post.  When the server replies, the result will go to
        // // onSubmitResponse
        // $.ajax({
        //   type: "GET",
        //   url: "/messages",
        //   dataType: "json",
        //   data: JSON.stringify({ mTitle: title, mMessage: msg }),
        //   success: CalendarEvents.onSubmitResponse
        // });
    };
    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a
     * result.
     *
     * @param data The object returned by the server
     */
    CalendarEvents.onSubmitResponse = function (data) {
        //     // If we get an "ok" message, clear the form and refresh the main
        //     // listing of messages
        //     if (data.mStatus === "ok") {
        //       ElementList.refresh();
        //     }
        //     // Handle explicit errors with a detailed popup message
        //     else if (data.mStatus === "error") {
        //       window.alert("The server replied with an error:\n" + data.mMessage);
        //     }
        //     // Handle other errors with a less-detailed popup message
        //     else {
        //       window.alert("Unspecified error");
        //     }
    };
    /**
     * The name of the DOM entry associated with CalendarEvents
     */
    CalendarEvents.NAME = "CalendarEvents";
    /**
     * Track if the Singleton has been initialized
     */
    CalendarEvents.isInit = false;
    return CalendarEvents;
}());
/// <reference path="ts/EditEntryForm.ts"/>
/// <reference path="ts/NewEntryForm.ts"/>
/// <reference path="ts/ElementList.ts"/>
/// <reference path="ts/Navbar.ts"/>
/// <reference path="ts/Login.ts"/>
/// <reference path="ts/Profile.ts"/>
/// <reference path="ts/Activity.ts"/>
/// <reference path="ts/CalendarEvents.ts"/>
// import express from "express";
// const app = express();
// const port = 8080;
// // define a route handler for the default home page
// app.get("/", (req, res) => {
//   res.send("Hello world!");
// });
// // start the Express server
// app.listen(port, () => {
//   console.log(`server started at http://localhost:${port}`);
// });
// Prevent compiler errors when using jQuery.  "$" will be given a type of
// "any", so that we can use it anywhere, and assume it has any fields or
// methods, without the compiler producing an error.
var $;
// Prevent compiler errors when using Handlebars
var Handlebars;
// a global for the EditEntryForm of the program
var login;
var profile;
// Run some configuration code when the web page loads
$(document).ready(function () {
    Navbar.refresh();
    NewEntryForm.refresh();
    CalendarEvents.refresh();
    Login.refresh();
    Profile.refresh();
    ElementList.refresh();
    EditEntryForm.refresh();
    Activity.refresh();
    // Create the object that controls the "Login" form
    login = new Login();
    profile = new Profile();
    // set up initial UI state
    $("#editElement").hide();
});
