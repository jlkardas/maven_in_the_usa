package edu.lehigh.cse216.jlk221.backend;

import java.util.*;
//import com.heroku.api.request.RequestConfig.Data;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    /*

    public void testLogin() {
        String db_url = env.get("DATABASE_URL");
        Map<String, String> env = System.getenv();
        final Database database = Database.getDatabase(db_url);
        
        int key = database.validateLogin("", "");
        if(key > 0){
            assertTrue(true);
        }
        else{
            assertTrue(false);
        }
    }

    public void testInsertComments() {
        try {
            String db_url = env.get("DATABASE_URL");
            Map<String, String> env = System.getenv();
            final Database database = Database.getDatabase(db_url);
            assertTrue(1 == database.insertComment("subject", "message"));
        } catch (Exception e) {
            assertTrue(false);
            e.printStackTrace();
        }
    }

    */
}
