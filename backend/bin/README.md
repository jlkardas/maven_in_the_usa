# Back-End Server

## Heroku CLI Login/Status

1. `heroku login`
2. `heroku apps`
   - mysterious-mountain-99747

## Deploy Heroku App

- `cd ../web; sh ldeploy.sh`
- `mvn package; mvn heroku:deploy`
  - Note: may have to run `heroku ps:scale web=1` to spin up a web dyno before deploying.
