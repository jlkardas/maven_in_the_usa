/// <reference path="ts/EditEntryForm.ts"/>
/// <reference path="ts/NewEntryForm.ts"/>
/// <reference path="ts/ElementList.ts"/>
/// <reference path="ts/Navbar.ts"/>
/// <reference path="ts/Login.ts"/>
/// <reference path="ts/Profile.ts"/>
/// <reference path="ts/Activity.ts"/>
/// <reference path="ts/CalendarEvents.ts"/>

// import express from "express";
// const app = express();
// const port = 8080;

// // define a route handler for the default home page
// app.get("/", (req, res) => {
//   res.send("Hello world!");
// });

// // start the Express server
// app.listen(port, () => {
//   console.log(`server started at http://localhost:${port}`);
// });

// Prevent compiler errors when using jQuery.  "$" will be given a type of
// "any", so that we can use it anywhere, and assume it has any fields or
// methods, without the compiler producing an error.
let $: any;

// Prevent compiler errors when using Handlebars
let Handlebars: any;

// a global for the EditEntryForm of the program
let login: Login;
let profile: Profile;

// Run some configuration code when the web page loads
$(document).ready(function() {
  Navbar.refresh();
  NewEntryForm.refresh();
  CalendarEvents.refresh();
  Login.refresh();
  Profile.refresh();
  ElementList.refresh();
  EditEntryForm.refresh();
  Activity.refresh();

  // Create the object that controls the "Login" form
  login = new Login();

  profile = new Profile();

  // set up initial UI state
  $("#editElement").hide();
});
