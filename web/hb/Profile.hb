<div id="Profile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-username">Profile</h4>
            </div>
            <div class="modal-body">
                {{!-- Display Username --}}
                <div class="profilePairStart">
                    <label class="profileText" for="Profile-username">Username:</label>
                    <label class="profileText" id="Profile-username">JohnDoe</label>
                </div>

                {{!-- Display User Email --}}
                <div class="profilePairStart">
                    <label class="profileText" for="Profile-email">Email:</label>
                    <label class="profileText" id="Profile-email">jid221@lehigh.edu</label>
                </div>

                <div class="profileSpacer"></div>

                {{!-- Display User Password --}}
                <div class="profilePairStart">
                    <label class="profileText" for="Profile-password">Password:</label>
                    <label class="profileText" id="Profile-password">ABC123!</label>
                </div>
                <div class="profilePairEven">
                    <input class="profileInput" type="text" placeholder="Edit Password"
                        id="Profile-edit-password"></input>
                    <button type="button" class="btn btn-default" id="Profile-submit-password">Save</button>
                </div>

                <div class="profileSpacer"></div>

                {{!-- Display User Comment --}}
                <div class="profilePairStart">
                    <label class="profileText" for="Profile-comment">Comment:</label>
                    <label class="profileTextFill" id="Profile-comment">Hello, this is my comment.</label>
                </div>
                <div class="profilePairEven">
                    <input class="profileInput" type="text" placeholder="Edit Comment"
                        id="Profile-edit-comment"></input>
                    <button type="button" class="btn btn-default" id="Profile-submit-comment">Save</button>
                </div>
            </div>
            <div class="modal-footer">
                {{!-- <button type="button" class="btn btn-default" id="Profile-Profile">Profile</button> --}}
                <button type="button" class="btn btn-default" id="Profile-Close">Close</button>
            </div>
        </div>
    </div>
</div>