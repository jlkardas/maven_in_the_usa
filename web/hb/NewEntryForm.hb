<div id="NewEntryForm" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Send a New Message</h4>
            </div>
            <div class="modal-body">
                <label for="NewEntryForm-title">Title</label>
                <input class="form-control" type="text" id="NewEntryForm-title" />
                <label for="NewEntryForm-message">Message</label>
                <textarea class="form-control" id="NewEntryForm-message"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="NewEntryForm-OK">Send</button>
                <button type="button" class="btn btn-default" id="NewEntryForm-Close">Cancel</button>
            </div>
        </div>
    </div>
</div>