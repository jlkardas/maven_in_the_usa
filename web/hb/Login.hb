<div id="Login" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-username">Login</h4>
            </div>
            <div class="modal-body">
                <label for="Login-email">Username</label>
                <input class="input" type="text" placeholder="User Email" id="Login-email" />
                <label for="Login-password">Password</label>
                <input class="input" type="text" placeholder="User Password" id="Login-password"></input>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="Login-Login">Login</button>
                <button type="button" class="btn btn-default" id="Login-Exit">Exit</button>
            </div>
        </div>
    </div>
</div>