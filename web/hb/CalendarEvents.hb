<div id="CalendarEvents" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Google Calendar Events</h4>
            </div>
            <div class="modal-body">
                <div id="userCalendarEvents"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="CalendarEvents-Close">Close</button>
            </div>
        </div>
    </div>
</div>