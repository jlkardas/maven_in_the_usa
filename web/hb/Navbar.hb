<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">The Buzz
    <img src="http://clipart-library.com/images_k/cute-bee-silhouette/cute-bee-silhouette-20.png" width="30" height="30"
      class="d-inline-block align-top" alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
    aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="navbarItem">
        <a class="btn btn-link" id="Navbar-add">
          Send Message
          <span class="glyphicon glyphicon-plus"></span><span class="sr-only">Show Trending Posts</span>
        </a>
      </li>
      <li class="navbarItem">
        <a class="btn btn-link" id="Navbar-calendar">
          View Calendar Events
        </a>
      </li>
      <li class="navbarItem">
        <a class="btn btn-link" id="Navbar-login">
          User Login
        </a>
      </li>
      <li class="navbarItem">
        <a class="btn btn-link" id="Navbar-profile">
          User Profile
        </a>
      </li>
      <li>
        <button id="signinButton">Sign in with Google</button>
        <script>
          $("#signinButton").click(function () {
            auth2.grantOfflineAccess().then(signInCallback);
          });
        </script>
      </li>
      <li>
        <button href="#" onclick="signOut();">Sign out</button>
        <script>
          function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
              console.log('User signed out.');
            });
          }
        </script>
      </li>
    </ul>
  </div>
</nav>