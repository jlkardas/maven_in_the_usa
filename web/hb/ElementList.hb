<div class="panel panel-default" id="ElementList">
    <div class="mainTitle">
        <h3>Messages</h3>
    </div>
    <table class="messageTable">
        <tbody class="messages">
            {{#each mData}}
            <tr class="messageEntry">
                {{!-- Message Title --}}
                <td class="messageTitle">{{this.mTitle}}</td>
                {{!-- Message Content --}}
                <td class="messageContent">{{this.mMessage}}</td>
                {{!-- Edit Button --}}
                <td><button class="ElementList-editbtn" data-value="{{this.mId}}"><i
                            class="fa fa-edit edit icon"></i></button>
                </td>
                {{!-- Delete Button --}}
                <td><button class="ElementList-delbtn" data-value="{{this.mId}}"><i class="fa fa-trash delete icon"></i>
                    </button>
                </td>
                {{!-- Upvote Button --}}
                <td><button class="ElementList-upbtn" data-value="{{this.mId}}"><i
                            class="fa fa-thumbs-up upvote icon"></i></button>
                </td>
                {{!-- Downvote Button --}}
                <td><button class="ElementList-downbtn" data-value="{{this.mId}}"><i
                            class="fa fa-thumbs-down downvote icon"></i></button>
            </tr>
            <tr class="messageComment">
                {{!-- TODO: show all message comments --}}
                {{#each mData.mComments}}
                <div>{{this}}</div>
                {{/each}}
                {{!-- Comment Box --}}
                <td><input class="commentBox" type="text"></td>
                {{!-- Comment Button --}}
                <td><button class="ElementList-cmtbtn" data-value="{{this.mId}}"><span>Comment</span></button></td>
            </tr>
            {{/each}}
        </tbody>
    </table>
</div>