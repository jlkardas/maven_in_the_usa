<div id="EditEntryForm" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Entry</h4>
            </div>
            <div class="modal-body">
                <label for="EditEntryForm-title">Edit Title</label>
                <input class="form-control" type="text" id="EditEntryForm-title" />
                <label for="EditEntryForm-message">Edit Message</label>
                <textarea class="form-control" id="EditEntryForm-message"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="EditEntryForm-OK">OK</button>
                <button type=" button" class="btn btn-default" id="EditEntryForm-Close">Close</button>
            </div>
        </div>
    </div>
</div>