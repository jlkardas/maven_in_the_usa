<div id = "Activity" class = "modal fade" role = "dialog">
    <div class="modal-dialog">
        <div class="modal-content">
	    <div class="modal-header">
       		<h4 class="modal-title">Message Comments</h4>
	    </div>
            <div class="modal-body">
		<label for = "Activity-comment">Comment</label>
		<textarea class = "form-control" id = "Activity-comment"></textarea>
	    </div>
	    <div class = "modal-footer">
		<button type="button" class="btn btn-default" id="Activity-OK">OK</button>
		<button type="button" class="btn btn-default" id="Activity-Close">Close</button>
	    </div>
	</div>
    </div>
</div>