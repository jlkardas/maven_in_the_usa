var describe: any;
var it: any;
var expect: any;
var $: any;

describe("Tests of basic math functions", function() {
  it("Adding 1 should work", function() {
    var foo = 0;
    foo += 1;
    expect(foo).toEqual(1);
  });

  it("Subtracting 1 should work", function() {
    var foo = 0;
    foo -= 1;
    expect(foo).toEqual(-1);
  });

  it("UI Test: Add Button Hides Listing", function() {
    // click the button for showing the add button
    $("#showFormButton").click();
    // expect that the add form is not hidden
    expect(
      $("#addElement")
        .attr("style")
        .indexOf("display: none;")
    ).toEqual(-1);
    // expect that the element listing is hidden
    expect(
      $("#showElements")
        .attr("style")
        .indexOf("display: none;")
    ).toEqual(0);
    // reset the UI, so we don't mess up the next test
    $("#addCancel").click();
  });

  it("UI Test: Edit Button Shows Listing", function() {
    // click the button for showing the edit button
    $(".editbtn").click();
    // expect that the add form is not hidden
    expect(
      $("#editElement")
        .attr("style")
        .indexOf("display: none;")
    ).toEqual(0);
    // expect that add element is hidden
    expect(
      $("#addElement")
        .attr("style")
        .indexOf("display: none;")
    ).toEqual(0);
    expect(
      $("#showElements")
        .attr("style")
        .indexOf("display: none;")
    ).toEqual(-1);
    // reset the UI, so we don't mess up the next test
    $("#addCancel").click();
  });
});
