class Profile {
  private static readonly NAME = "Profile";
  private static isInit = false;

  private static init() {
    if (!Profile.isInit) {
      $("body").append(Handlebars.templates[Profile.NAME + ".hb"]());
      //   $("#" + Profile.NAME + "-Profile").click(Profile.submitProfile);
      $("#" + Profile.NAME + "-Close").click(Profile.hide);
      Profile.isInit = true;
    }
  }

  public static refresh() {
    Profile.init();
  }

  private static hide() {
    $("#" + Profile.NAME + "-username").val("");
    $("#" + Profile.NAME + "-email").val("");
    $("#" + Profile.NAME + "-password").val("");
    $("#" + Profile.NAME + "-comment").val("");
    $("#" + Profile.NAME).modal("hide");
  }

  public static show() {
    $("#" + Profile.NAME + "-username").val("");
    $("#" + Profile.NAME + "-email").val("");
    $("#" + Profile.NAME + "-password").val("");
    $("#" + Profile.NAME + "-edit-password").val("");
    $("#" + Profile.NAME + "-comment").val("");
    $("#" + Profile.NAME + "-edit-comment").val("");
    $("#" + Profile.NAME + "-Success").hide();
    $("#" + Profile.NAME).modal("show");
  }

}
