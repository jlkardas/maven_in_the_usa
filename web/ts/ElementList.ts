class ElementList {
  private static readonly NAME = "ElementList";
  private static isInit = false;

  private static init() {
    if (!ElementList.isInit) {
      ElementList.isInit = true;
    }
  }

  // Refresh method to initialize the elementList and get the messages from the server
  public static refresh() {
    // Make sure the singleton is initialized
    ElementList.init();
    // Issue a GET, and then pass the result to update()
    $.ajax({
      type: "GET",
      url: "/messages",
      dataType: "json",
      success: (response: JSON) => {
        console.log(response);
        ElementList.update(response);
      }
    });
  }

  private static update(data: any) {
    // Remove the table of data, if it exists
    $("#" + ElementList.NAME).remove();
    // Use a template to re-generate the table, and then insert it
    $("body").append(Handlebars.templates[ElementList.NAME + ".hb"](data));
    // Find all of the Edit buttons, and set their behavior
    $("." + ElementList.NAME + "-editbtn").click(ElementList.clickEdit);
    // Find all of the delete buttons, and set their behavior
    $("." + ElementList.NAME + "-delbtn").click(ElementList.clickDelete);
    // Find all of the upvote buttons, and set their behavior
    $("." + ElementList.NAME + "-upbtn").click(ElementList.clickUpvote);
    // Find all of the downvote buttons, and set their behavior
    $("." + ElementList.NAME + "-downbtn").click(ElementList.clickDownvote);
    // Find all of the downvote buttons, and set their behavior
    $("." + ElementList.NAME + "-cmtbtn").click(ElementList.sendComment);
    // Find all of the comments
    $("." + ElementList.NAME + "-actvbtn").click(ElementList.activity);
  }

  // Delete button handler
  private static clickDelete() {
    let id = $(this).data("value");
    $.ajax({
      type: "DELETE",
      url: "/messages/" + id,
      dataType: "json",
      // TODO: we should really have a function that looks at the return
      //       value and possibly prints an error message.
      success: ElementList.refresh
    });
  }

  // Edit button handler
  private static clickEdit() {
    let id = $(this).data("value");
    $.ajax({
      type: "GET",
      url: "/messages/" + id,
      dataType: "json",
      success: EditEntryForm.show(id)
    });
  }

  // Upvote button handler
  private static clickUpvote() {
    let id = $(this).data("value");
    $.ajax({
      type: "PUT",
      url: "/messages/" + id + "/upvotes",
      dataType: "json",
      success: ElementList.refresh
    });
  }

  // Downvote button handler
  private static clickDownvote() {
    let id = $(this).data("value");
    $.ajax({
      type: "PUT",
      url: "/messages/" + id + "/downvotes",
      dataType: "json",
      success: ElementList.refresh
    });
  }

  /*
    sendComment is called when the user clicks on the "Comment" button
  */
  private static sendComment() {
    let id = $(this).data("value");
    console.log("PUT comment for id " + id + " : " + $(".commentBox").val());
    $.ajax({
      type: "PUT",
      url: "/messages/" + id + "/comments",
      dataType: "json",
      data: { comment: $(".commentBox").val() },
      success: (response: JSON) => {
        ElementList.refresh;
      }
    });
  }

  /*
    activity is called when the user clicks on the "Activity" button
  */
  private static activity() {
    let id = $(this).data("value");
    $.ajax({
      type: "GET",
      url: "/messages/" + id + "/comments",
      dataType: "json",
      success: Activity.show(id)
    });
  }

}
