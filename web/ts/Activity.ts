class Activity {
    private static readonly NAME = "Activity";
  
    private static isInit = false;
  
    private static id = NaN;
  
    private static init() {
      if (!Activity.isInit) {
        $("body").append(Handlebars.templates[Activity.NAME + ".hb"]());
        $("#" + Activity.NAME + "-OK").click(Activity.submitForm);
        $("#" + Activity.NAME + "-Close").click(Activity.hide);
        Activity.isInit = true;
      }
    }
  
    public static refresh() {
      Activity.init();
    }
  
    private static hide() {
      $("#" + Activity.NAME + "-title").val("");
      $("#" + Activity.NAME + "-message").val("");
      $("#" + Activity.NAME + "-comment").val("");
      $("#" + Activity.NAME).modal("hide");
    }
  
    public static show(id: any) {
      Activity.id = id;
      $("#" + Activity.NAME + "-title").val("");
      $("#" + Activity.NAME + "-message").val("");
      $("#" + Activity.NAME + "-comment").val("");
      $("#" + Activity.NAME).modal("show");
    }
  
    private static submitForm() {
      // get the values of the two fields, force them to be strings, and check
      // that neither is empty
      let title = "" + $("#" + Activity.NAME + "-title").val();
      let msg = "" + $("#" + Activity.NAME + "-message").val();
      let comment = "" + $("#" + Activity.NAME + "-comment").val();
      // NB: we assume that the user didn't modify the value of #editId
      if (title === "" || msg === "" || comment === "") {
        window.alert("Error: title or message is not valid");
        return;
      }
      // set up an AJAX post.  When the server replies, the result will go to
      // onSubmitResponse
      $.ajax({
        type: "POST",
        url: "/messages/" + Activity.id + "/comment/",
        dataType: "json",
        data: JSON.stringify({ mTitle: title, mMessage: msg, mComment: comment }),
        success: (response: JSON) => {
          Activity.onSubmitResponse(response);
        }
      });
    }
  
    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a
     * result.
     *
     * @param data The object returned by the server
     */
    private static onSubmitResponse(data: any) {
      // If we get an "ok" message, clear the form and refresh the main
      // listing of messages
      if (data.mStatus === "ok") {
        Activity.hide();
        Activity.refresh();
      }
      // Handle explicit errors with a detailed popup message
      else if (data.mStatus === "error") {
        window.alert("The server replied with an error:\n" + data.mMessage);
      }
      // Handle other errors with a less-detailed popup message
      else {
        window.alert("Unspecified error");
      }
    }
  }