// import { google } from "googleapis";

// const calendar = google.calendar_v3;
// const SCOPES = ["https://www.googleapis.com/auth/calendar.readonly"];

/**
 * CalendarEvents encapsulates all of the code for the form for adding an entry
 */
class CalendarEvents {
  /**
   * The name of the DOM entry associated with CalendarEvents
   */
  private static readonly NAME = "CalendarEvents";

  /**
   * Track if the Singleton has been initialized
   */
  private static isInit = false;

  /**
   * Initialize the CalendarEvents by creating its element in the DOM and
   * configuring its buttons.  This needs to be called from any public static
   * method, to ensure that the Singleton is initialized before use
   */
  private static init() {
    if (!CalendarEvents.isInit) {
      $("body").append(Handlebars.templates[CalendarEvents.NAME + ".hb"]());
      $("#" + CalendarEvents.NAME + "-Close").click(CalendarEvents.hide);
      CalendarEvents.isInit = true;
    }
  }

  /**
   * Refresh() doesn't really have much meaning, but just like in sNavbar, we
   * have a refresh() method so that we don't have front-end code calling
   * init().
   */
  public static refresh() {
    CalendarEvents.init();
  }

  /**
   * Hide the CalendarEvents.  Be sure to clear its fields first
   */
  private static hide() {
    // $("#" + CalendarEvents.NAME + "-title").val("");
    // $("#" + CalendarEvents.NAME + "-message").val("");
    $("#" + CalendarEvents.NAME).modal("hide");
  }

  /**
   * Show the CalendarEvents.  Be sure to clear its fields, because there are
   * ways of making a Bootstrap modal disapper without clicking Close, and
   * we haven't set up the hooks to clear the fields on the events associated
   * with those ways of making the modal disappear.
   */
  public static show() {
    // $("#" + CalendarEvents.NAME + "-title").val("");
    // $("#" + CalendarEvents.NAME + "-message").val("");
    $("#" + CalendarEvents.NAME).modal("show");
  }

  /**
   * Send data to submit the form only if the fields are both valid.
   * Immediately hide the form when we send data, so that the user knows that
   * their click was received.
   */
  private static submitForm() {
    // get the values of the two fields, force them to be strings, and check
    // that neither is empty
    // let title = "" + $("#" + CalendarEvents.NAME + "-title").val();
    // let msg = "" + $("#" + CalendarEvents.NAME + "-message").val();
    // if (title === "" || msg === "") {
    //   window.alert("Error: title or message is not valid");
    //   return;
    // }
    // CalendarEvents.hide();
    // // set up an AJAX post.  When the server replies, the result will go to
    // // onSubmitResponse
    // $.ajax({
    //   type: "GET",
    //   url: "/messages",
    //   dataType: "json",
    //   data: JSON.stringify({ mTitle: title, mMessage: msg }),
    //   success: CalendarEvents.onSubmitResponse
    // });
  }

  /**
   * onSubmitResponse runs when the AJAX call in submitForm() returns a
   * result.
   *
   * @param data The object returned by the server
   */
  private static onSubmitResponse(data: any) {
    //     // If we get an "ok" message, clear the form and refresh the main
    //     // listing of messages
    //     if (data.mStatus === "ok") {
    //       ElementList.refresh();
    //     }
    //     // Handle explicit errors with a detailed popup message
    //     else if (data.mStatus === "error") {
    //       window.alert("The server replied with an error:\n" + data.mMessage);
    //     }
    //     // Handle other errors with a less-detailed popup message
    //     else {
    //       window.alert("Unspecified error");
    //     }
  }
}
