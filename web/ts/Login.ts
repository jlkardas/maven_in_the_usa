class Login {
  private static readonly NAME = "Login";
  private static isInit = false;

  private static init() {
    if (!Login.isInit) {
      $("body").append(Handlebars.templates[Login.NAME + ".hb"]());
      $("#" + Login.NAME + "-Login").click(Login.submitLogin);
      $("#" + Login.NAME + "-Exit").click(Login.hide);
      Login.isInit = true;
    }
  }

  public static refresh() {
    Login.init();
  }

  private static hide() {
    $("#" + Login.NAME + "-username").val("");
    $("#" + Login.NAME + "-password").val("");
    $("#" + Login.NAME).modal("hide");
  }

  public static show() {
    $("#" + Login.NAME + "-username").val("");
    $("#" + Login.NAME + "-password").val("");
    $("#" + Login.NAME + "-Success").hide();
    $("#" + Login.NAME).modal("show");
  }

  // frontend sends backend get request with email of user loging in
  private static submitLogin() {
    let e = $("#" + Login.NAME + "-email").val();
    let p = $("#" + Login.NAME + "-password").val();
    //console.log("Email: " + u);
    //console.log("Password: " + p);
    if (e === "" || p === "") {
      window.alert("Error: title or message is not valid");
      return;
    }
    Login.hide();
    $.ajax({
      type: "PUT",
      url: /*backendURL +*/ "/users/" + e,
      dataType: "json",
      success: (response: JSON) => {
        console.log(response);
        // TODO: if the resposne is good, update the Profile to show
        // any releveant information associated with the user
        $("#Profile-username").val("");
        $("#Profile-email").val("");
        $("#Profile-password").val("");
        $("#Profile-comment").val("");
      }
    });

    /* PREVIOUS CODE
    $.ajax({
      type: "GET",
      url: "/users/" + e,
      dataType: "json",
      success: (response: JSON) => {
        console.log(response);
        // TODO: if the resposne is good, update the Profile to show
        // any releveant information associated with the user
        $("#Profile-username").val("");
        $("#Profile-email").val("");
        $("#Profile-password").val("");
        $("#Profile-comment").val("");
        Login.hide();
      }
    });
    */
  }
}
