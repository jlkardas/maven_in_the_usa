class EditEntryForm {
  private static readonly NAME = "EditEntryForm";

  private static isInit = false;

  private static id = NaN;

  private static init() {
    if (!EditEntryForm.isInit) {
      $("body").append(Handlebars.templates[EditEntryForm.NAME + ".hb"]());
      $("#" + EditEntryForm.NAME + "-OK").click(EditEntryForm.submitForm);
      $("#" + EditEntryForm.NAME + "-Close").click(EditEntryForm.hide);
      EditEntryForm.isInit = true;
    }
  }

  public static refresh() {
    EditEntryForm.init();
  }

  private static hide() {
    $("#" + EditEntryForm.NAME + "-title").val("");
    $("#" + EditEntryForm.NAME + "-message").val("");
    $("#" + EditEntryForm.NAME).modal("hide");
  }

  public static show(id: any) {
    EditEntryForm.id = id;
    $("#" + EditEntryForm.NAME + "-title").val("");
    $("#" + EditEntryForm.NAME + "-message").val("");
    $("#" + EditEntryForm.NAME).modal("show");
  }

  private static clearForm() {
    $("#editTitle").val("");
    $("#editMessage").val("");
    $("#editId").val("");
    $("#editCreated").text("");

    // reset the UI
    $("#addElement").hide();
    $("#editElement").hide();
    $("#showElements").show();
  }

  private static submitForm() {
    // get the values of the two fields, force them to be strings, and check
    // that neither is empty
    let title = "" + $("#" + EditEntryForm.NAME + "-title").val();
    let msg = "" + $("#" + EditEntryForm.NAME + "-message").val();
    // NB: we assume that the user didn't modify the value of #editId
    if (title === "" || msg === "") {
      window.alert("Error: title or message is not valid");
      return;
    }
    // set up an AJAX post.  When the server replies, the result will go to
    // onSubmitResponse
    $.ajax({
      type: "PUT",
      url: "/messages/" + EditEntryForm.id,
      dataType: "json",
      data: JSON.stringify({ mTitle: title, mMessage: msg }),
      success: (response: JSON) => {
        EditEntryForm.onSubmitResponse(response);
      }
    });
  }

  /**
   * onSubmitResponse runs when the AJAX call in submitForm() returns a
   * result.
   *
   * @param data The object returned by the server
   */
  private static onSubmitResponse(data: any) {
    // If we get an "ok" message, clear the form and refresh the main
    // listing of messages
    if (data.mStatus === "ok") {
      EditEntryForm.clearForm();
      EditEntryForm.hide();
      ElementList.refresh();
    }
    // Handle explicit errors with a detailed popup message
    else if (data.mStatus === "error") {
      window.alert("The server replied with an error:\n" + data.mMessage);
    }
    // Handle other errors with a less-detailed popup message
    else {
      window.alert("Unspecified error");
    }
  }
}
