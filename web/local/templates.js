(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['ElementList.hb'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "            <tr class=\"messageEntry\">\n                <td class=\"messageTitle\">"
    + alias2(alias1((depth0 != null ? depth0.mTitle : depth0), depth0))
    + "</td>\n                <td class=\"messageContent\">"
    + alias2(alias1((depth0 != null ? depth0.mMessage : depth0), depth0))
    + "</td>\n                <td><button class=\"ElementList-editbtn\" data-value=\""
    + alias2(alias1((depth0 != null ? depth0.mId : depth0), depth0))
    + "\"><i\n                            class=\"fa fa-edit edit icon\"></i></button>\n                </td>\n                <td><button class=\"ElementList-delbtn\" data-value=\""
    + alias2(alias1((depth0 != null ? depth0.mId : depth0), depth0))
    + "\"><i class=\"fa fa-trash delete icon\"></i>\n                    </button>\n                </td>\n                <td><button class=\"ElementList-upbtn\" data-value=\""
    + alias2(alias1((depth0 != null ? depth0.mId : depth0), depth0))
    + "\"><i\n                            class=\"fa fa-thumbs-up upvote icon\"></i></button>\n                </td>\n                <td><button class=\"ElementList-downbtn\" data-value=\""
    + alias2(alias1((depth0 != null ? depth0.mId : depth0), depth0))
    + "\"><i\n                            class=\"fa fa-thumbs-down downvote icon\"></i></button>\n            </tr>\n            <tr class=\"messageComment\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.mData : depth0)) != null ? stack1.mComments : stack1),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":16},"end":{"line":33,"column":25}}})) != null ? stack1 : "")
    + "                <td><input class=\"commentBox\" type=\"text\"></td>\n                <td><button class=\"ElementList-cmtbtn\" data-value=\""
    + alias2(alias1((depth0 != null ? depth0.mId : depth0), depth0))
    + "\"><span>Comment</span></button></td>\n            </tr>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "                <div>"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"panel panel-default\" id=\"ElementList\">\n    <div class=\"mainTitle\">\n        <h3>Messages</h3>\n    </div>\n    <table class=\"messageTable\">\n        <tbody class=\"messages\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.mData : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":12},"end":{"line":39,"column":21}}})) != null ? stack1 : "")
    + "        </tbody>\n    </table>\n</div>";
},"useData":true});
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['EditEntryForm.hb'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div id=\"EditEntryForm\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Edit Entry</h4>\n            </div>\n            <div class=\"modal-body\">\n                <label for=\"EditEntryForm-title\">Edit Title</label>\n                <input class=\"form-control\" type=\"text\" id=\"EditEntryForm-title\" />\n                <label for=\"EditEntryForm-message\">Edit Message</label>\n                <textarea class=\"form-control\" id=\"EditEntryForm-message\"></textarea>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-default\" id=\"EditEntryForm-OK\">OK</button>\n                <button type=\" button\" class=\"btn btn-default\" id=\"EditEntryForm-Close\">Close</button>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['NewEntryForm.hb'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div id=\"NewEntryForm\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Send a New Message</h4>\n            </div>\n            <div class=\"modal-body\">\n                <label for=\"NewEntryForm-title\">Title</label>\n                <input class=\"form-control\" type=\"text\" id=\"NewEntryForm-title\" />\n                <label for=\"NewEntryForm-message\">Message</label>\n                <textarea class=\"form-control\" id=\"NewEntryForm-message\"></textarea>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-default\" id=\"NewEntryForm-OK\">Send</button>\n                <button type=\"button\" class=\"btn btn-default\" id=\"NewEntryForm-Close\">Cancel</button>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Login.hb'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div id=\"Login\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-username\">Login</h4>\n            </div>\n            <div class=\"modal-body\">\n                <label for=\"Login-email\">Username</label>\n                <input class=\"input\" type=\"text\" placeholder=\"User Email\" id=\"Login-email\" />\n                <label for=\"Login-password\">Password</label>\n                <input class=\"input\" type=\"text\" placeholder=\"User Password\" id=\"Login-password\"></input>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-default\" id=\"Login-Login\">Login</button>\n                <button type=\"button\" class=\"btn btn-default\" id=\"Login-Exit\">Exit</button>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Navbar.hb'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\n  <a class=\"navbar-brand\" href=\"#\">The Buzz\n    <img src=\"http://clipart-library.com/images_k/cute-bee-silhouette/cute-bee-silhouette-20.png\" width=\"30\" height=\"30\"\n      class=\"d-inline-block align-top\" alt=\"\">\n  </a>\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\" aria-controls=\"navbarNav\"\n    aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n  <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\n    <ul class=\"navbar-nav\">\n      <li class=\"navbarItem\">\n        <a class=\"btn btn-link\" id=\"Navbar-add\">\n          Send Message\n          <span class=\"glyphicon glyphicon-plus\"></span><span class=\"sr-only\">Show Trending Posts</span>\n        </a>\n      </li>\n      <li class=\"navbarItem\">\n        <a class=\"btn btn-link\" id=\"Navbar-calendar\">\n          View Calendar Events\n        </a>\n      </li>\n      <li class=\"navbarItem\">\n        <a class=\"btn btn-link\" id=\"Navbar-login\">\n          User Login\n        </a>\n      </li>\n      <li class=\"navbarItem\">\n        <a class=\"btn btn-link\" id=\"Navbar-profile\">\n          User Profile\n        </a>\n      </li>\n      <li>\n        <button id=\"signinButton\">Sign in with Google</button>\n        <script>\n          $(\"#signinButton\").click(function () {\n            auth2.grantOfflineAccess().then(signInCallback);\n          });\n        </script>\n      </li>\n      <li>\n        <button href=\"#\" onclick=\"signOut();\">Sign out</button>\n        <script>\n          function signOut() {\n            var auth2 = gapi.auth2.getAuthInstance();\n            auth2.signOut().then(function () {\n              console.log('User signed out.');\n            });\n          }\n        </script>\n      </li>\n    </ul>\n  </div>\n</nav>";
},"useData":true});
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Profile.hb'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div id=\"Profile\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-username\">Profile</h4>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"profilePairStart\">\n                    <label class=\"profileText\" for=\"Profile-username\">Username:</label>\n                    <label class=\"profileText\" id=\"Profile-username\">JohnDoe</label>\n                </div>\n\n                <div class=\"profilePairStart\">\n                    <label class=\"profileText\" for=\"Profile-email\">Email:</label>\n                    <label class=\"profileText\" id=\"Profile-email\">jid221@lehigh.edu</label>\n                </div>\n\n                <div class=\"profileSpacer\"></div>\n\n                <div class=\"profilePairStart\">\n                    <label class=\"profileText\" for=\"Profile-password\">Password:</label>\n                    <label class=\"profileText\" id=\"Profile-password\">ABC123!</label>\n                </div>\n                <div class=\"profilePairEven\">\n                    <input class=\"profileInput\" type=\"text\" placeholder=\"Edit Password\"\n                        id=\"Profile-edit-password\"></input>\n                    <button type=\"button\" class=\"btn btn-default\" id=\"Profile-submit-password\">Save</button>\n                </div>\n\n                <div class=\"profileSpacer\"></div>\n\n                <div class=\"profilePairStart\">\n                    <label class=\"profileText\" for=\"Profile-comment\">Comment:</label>\n                    <label class=\"profileTextFill\" id=\"Profile-comment\">Hello, this is my comment.</label>\n                </div>\n                <div class=\"profilePairEven\">\n                    <input class=\"profileInput\" type=\"text\" placeholder=\"Edit Comment\"\n                        id=\"Profile-edit-comment\"></input>\n                    <button type=\"button\" class=\"btn btn-default\" id=\"Profile-submit-comment\">Save</button>\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-default\" id=\"Profile-Close\">Close</button>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Activity.hb'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div id = \"Activity\" class = \"modal fade\" role = \"dialog\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n	    <div class=\"modal-header\">\n       		<h4 class=\"modal-title\">Message Comments</h4>\n	    </div>\n            <div class=\"modal-body\">\n		<label for = \"Activity-comment\">Comment</label>\n		<textarea class = \"form-control\" id = \"Activity-comment\"></textarea>\n	    </div>\n	    <div class = \"modal-footer\">\n		<button type=\"button\" class=\"btn btn-default\" id=\"Activity-OK\">OK</button>\n		<button type=\"button\" class=\"btn btn-default\" id=\"Activity-Close\">Close</button>\n	    </div>\n	</div>\n    </div>\n</div>";
},"useData":true});
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['CalendarEvents.hb'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div id=\"CalendarEvents\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">Google Calendar Events</h4>\n            </div>\n            <script>\n                function signInCallback(authResult) {\n                    if (authResult[\"code\"]) {\n                        // Hide the sign-in button now that the user is authorized, for example:\n                        $(\"#signinButton\").attr(\"style\", \"display: none\");\n\n                        // Send the code to the server\n                        $.ajax({\n                            type: \"POST\",\n                            url: \"/authcode\",\n                            // Always include an `X-Requested-With` header in every AJAX request,\n                            // to protect against CSRF attacks.\n                            headers: {\n                                \"X-Requested-With\": \"XMLHttpRequest\"\n                            },\n                            contentType: \"application/octet-stream; charset=utf-8\",\n                            success: function (result) {\n                                // result = accessToken\n                                console.log(result);\n                                // $(\"#userCalendarEvents\").val(result);\n                                document.getElementById(\"userCalendarEvents\").innerHTML = \"Events...\";\n                            },\n                            processData: false,\n                            data: authResult[\"code\"]\n                        });\n                    } else {\n                        console.log(\"error authorizing with google!\");\n                    }\n                }\n            </script>\n            <div class=\"modal-body\">\n                <div id=\"userCalendarEvents\"></div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-default\" id=\"CalendarEvents-Close\">Close</button>\n            </div>\n        </div>\n    </div>\n</div>";
},"useData":true});
})();
