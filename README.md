# Maven in the USA #

## Backend Routes: ##
* Messages:
	- .../messages/id/upvotes 
		- individual message upvotes
	- .../messages/id/downvotes 
		- individual message downvotes
	- .../messages/id 
		- individual messages, including message content, this route should also be used to edit or delete a message
	- .../messages 
		- all messages

* Users:
	- .../users/ 
		- all users
	- .../users/email
		- should return the id of the user with given email
	- .../users/id/email 
		- individual user email
	- .../users/id/comment 
		- individual user comment
	- .../users/id/password 
		- individual user password

mData: {
	.mTitle
	.mData
	.mId
	.mComments[]
}