package maven_in_the_USA.cse216.lehigh.edu.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
//import android.widget.Toolbar;

//import volley dependencies individually
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Request;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    /**
     * msg_ids holds the data returned from /messages
     * msg_contents holds the data returned from /messages/id
     */
    ArrayList<Datum> msg_ids = new ArrayList<>();
    int num_msg_ids = 0;
    ArrayList<Entry> msg_contents = new ArrayList<>();

    String title = null;
    String message = null;

    RecyclerView rv;
    ItemListAdapter adapter;

    private TextView thedate;
    private Button btngocalendar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        thedate = (TextView) findViewById(R.id.date);
        btngocalendar = (Button) findViewById(R.id.btngocalendar);
        String date = incoming.getStringExtra("date");
        thedate.setText(date);

        btngocalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,CalendarActivity.class);
                startActivity(intent);
            }
        });

        /* Toolbar */
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.d("jlk221", "Debug Message from onCreate");

        /* Singleton Volley Implementation */
        RequestQueue queue = SingletonVolley.getInstance(this.getApplicationContext()).getRequestQueue();


        String messages_uri = "https://mysterious-mountain-99747.herokuapp.com/messages";

        // Request a string response from the provided URL.
        StringRequest get_msg_ids = new StringRequest(Request.Method.GET, messages_uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        parseIds(response, msg_ids);
                        for(int i = 0; i < num_msg_ids; i++) {
                            getMessage(msg_ids.get(i).getmId());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("jlk221", "That didn't work!");
            }
        });

        // Add the request to the RequestQueue.
        queue.add(get_msg_ids);


        final EditText etTitle = (EditText) findViewById(R.id.title_text);
        final EditText etMessage = (EditText) findViewById(R.id.message_text);
        Button bSend = (Button) findViewById(R.id.send_button);
        bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etTitle.getText().toString().equals("") && !etMessage.getText().toString().equals("")) {
                    title = etTitle.getText().toString();
                    message = etMessage.getText().toString();

                    sendMessage(title, message);

                    etTitle.setText("");
                    etMessage.setText("");

                    // The following lines of code hide the keyboard after the send button is clicked
                    InputMethodManager inputManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                }
            }
        });

        final EditText comment = (EditText) findViewById(R.id.commentText);
        Button bComment = (Button) findViewById(R.id.commentButton);
        bComment.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!comment.getText().toString().equals("")) {
                Intent i = new Intent();
                i.putExtra("message", comment.getText().toString());
                //sendCommentRequest(id put URL here );
                setResult(Activity.RESULT_OK, i);
                finish(); // makes it go bck to the old activity
            }
        }
        });
    
    }

//This was my attempt at comments. It doesnt work but I think it's not complete trash
//private void sendCommentRequest(String commPostURL) {
//    RequestQueue commentQueue = VolleySingleton.getInstance(this).getRequestQueue(); // something not working here --> printed "that didn't work"
//
//    // Request a string response from the provided URL.
//    // post
//    final StringRequest stringCommentRequest = new StringRequest(Request.Method.POST, commPostURL,
//        new Response.Listener<String>() {
//        @Override
//        public void onResponse(String response) {
//
//        Log.d("comment error", "Response is: " + response);
//        }
//    }, new Response.ErrorListener() {
//    @Override
//    public void onErrorResponse(VolleyError error) {
//        Log.e("debugging", error.getMessage());
//    }
//    });
//    // Add the request to the RequestQueue.
//    commentQueue.add(stringCommentRequest);
//}


    /*
        getMessage sends a GET request to uri,
        calls parseMessage to get a string from the json response,
        and sets the recycler view element text to the string contents

        @param id, the id of the message to append to the uri of the GET request
     */
    private void getMessage(int id) {
        String uri = "https://mysterious-mountain-99747.herokuapp.com/messages/";
        String id_uri = uri.concat(String.valueOf(id));

        StringRequest get_msg_content = new StringRequest(Request.Method.GET, id_uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        parseMessage(response, msg_contents);
//                        Collections.sort(msg_contents, new Comparator<Entry>() {
//                            @Override
//                            public int compare(Entry lhs, Entry rhs) {
//                                if(lhs.getId() < rhs.getId()) {
//                                    return 1;
//                                }
//                                else if(lhs.getId() > rhs.getId()) {
//                                    return 0;
//                                }
//                                return -1;
//                            }
//                        });
                        updateRecycler();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("jlk221", "Error genereating string request");
            }
        });

        SingletonVolley.getInstance(this.getApplicationContext()).addToRequestQueue(get_msg_content);

//        adapter.setClickListener(new ItemListAdapter.ClickListener() {
//            @Override
//            public void onClick(Entry e) {
//                Toast.makeText(MainActivity.this, e.mId + " : " + e.mTitle, Toast.LENGTH_LONG).show();
//            }
//        });

    }

    private void updateRecycler(){
        rv = (RecyclerView) findViewById(R.id.msg_list_view);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ItemListAdapter(this, msg_contents);
        rv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        rv.scrollToPosition(adapter.getItemCount() - 1);
    }

    private void parseIds(String response, ArrayList<Datum> list){
        try {
            //create a json object of the form {"mStatus":"","mData":[{"mId":0,"mTitle":"Title 0"}, ...]}
            JSONObject json = new JSONObject(response);
            //parse message data array from response
            JSONArray data = json.getJSONArray("mData");
            //parse message ID and Title from each entry in mData
            for(int i = 0; i < data.length(); ++i) {
                int id = data.getJSONObject(i).getInt("mId");
                String title = data.getJSONObject(i).getString("mTitle");
                int likes = data.getJSONObject(i).getInt("mLike");
                int dislikes = data.getJSONObject(i).getInt("mDislike");

                //add message entry to ArrayList of Datum
                list.add(new Datum(id, title, likes, dislikes));
                num_msg_ids++;
                Log.d("jlk221", "Added { Id: [" + String.valueOf(id) + "] Title: \"" + list.get(i).mTitle + "\"} to msg_ids");

            }
        } catch (final JSONException e) {
            Log.d("jlk221", "Error parsing message ids: " + e.getMessage());
            return;
        }
        Log.d("jlk221", "Successfully retreived ids");
    }

    private void parseMessage(String response, ArrayList<Entry> list) {
        try {
            //create a json object of the form {"mStatus":"ok","mData":{"mId":0,"mTitle":"Message","mContent":"Body","mCreated":"Sep 21, 2019 1:20:50 AM"}}
            JSONObject json = new JSONObject(response);
            //parse message data array from response
            Log.d("jsondata", String.valueOf(json));
            JSONObject data = json.getJSONObject("mData");
            Log.d("jsondata", String.valueOf(data));
            //parse message ID and Title from each entry in mData
            int id = data.getInt("mId");
            String title = data.getString("mTitle");
<<<<<<< HEAD
            //String content = data.getString("mContent");
           //String created = data.getString("mCreated");
            int likes = data.getInt("mLike");
            int dislikes = data.getInt("mDislike");

            //add message entry to ArrayList of Datum
            //list.add(new Entry(id, title, content, created, likes, dislikes));
            list.add(new Entry(id, title, likes, dislikes));
            //Log.d("jlk221", "Added { Id: [" + String.valueOf(id) + "], Title: \"" + list.get(list.size() - 1).mTitle + "\", Contents: \"" + list.get(list.size() - 1).mContent + "\" } to msg_contents");
            Log.d("jlk221", "Added { Id: [" + String.valueOf(id) + "], Title: \"" + list.get(list.size() - 1).mTitle + "\"} to msg_contents");
=======
//            String content = data.getString("mContent");
            //String created = data.getString("mCreated");

            //add message entry to ArrayList of Datum
//            list.add(new Entry(id, title, content, created));
            list.add(new Entry(id, title));
            updateRecycler();
            //Log.d("jlk221", "Added { Id: [" + String.valueOf(id) + "], Title: \"" + list.get(list.size() - 1).mTitle + "\", Contents: \"" + list.get(list.size() - 1).mContent + "\" } to msg_contents");
>>>>>>> android
        } catch (final JSONException e) {
            Log.d("jlk221", "Error parsing message entry: " + e.getMessage());
            return;
        }
        Log.d("jlk221", "Successfully retrieved message entry.");
    }


    private void sendMessage(String title, String message) {
        try {
            String URL = "https://mysterious-mountain-99747.herokuapp.com/messages";
            JSONObject newMessage = new JSONObject();

            newMessage.put("mTitle", title);
            newMessage.put("mMessage", message);

            JsonObjectRequest jsonObject = new JsonObjectRequest(Request.Method.POST, URL, newMessage, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(getApplicationContext(), "Response:  " + response.toString(), Toast.LENGTH_SHORT).show();
                    //updateRecycler();
                    try {
                        String mStatus = response.getString("mStatus");
                        int mId = response.getInt("mMessage");
                        getMessage(mId);
                    }
                    catch (final JSONException e) {
                        Log.d("jlk221", "Error parsing message entry: " + e.getMessage());
                        return;
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    onBackPressed();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    final Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Basic " + "c2FnYXJAa2FydHBheS5jb206cnMwM2UxQUp5RnQzNkQ5NDBxbjNmUDgzNVE3STAyNzI=");//put your token here
                    return headers;
                }
            };
            SingletonVolley.getInstance(this.getApplicationContext()).addToRequestQueue(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Toast.makeText(getApplicationContext(), "done", Toast.LENGTH_LONG).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent i = new Intent(getApplicationContext(), SecondActivity.class);
            i.putExtra("label_contents", "CSE216 is the best");
            startActivityForResult(i, 789); // 789 is the number that will come back to us
            return true;
        }
        else if(id == R.id.action_pic) {
            Intent i = new Intent(getApplicationContext(), PhotoActivity.class);
            startActivityforResult(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 789) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // Get the "extra" string of data
                Toast.makeText(MainActivity.this, data.getStringExtra("result"), Toast.LENGTH_LONG).show();
                //updateRecycler();
            }
        }
    }



}
