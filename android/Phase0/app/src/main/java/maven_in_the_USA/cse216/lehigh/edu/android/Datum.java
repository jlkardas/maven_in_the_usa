package maven_in_the_USA.cse216.lehigh.edu.android;

/**
 * Created by jessekardas on 9/8/19.
 */

class Datum {
    /**
     * An integer index for this piece of data
     */
    int mId;

    /**
     * The string contents that comprise this piece of data
     */
    String mTitle;

    /**
     * Number of likes a message has
     */
    int mLike;

    /**
     * Number of dislikes a message has
     */
    int mDislike;



    /**
     * Construct a Datum by setting its index and text
     *
     * @param id The index of this piece of data
     * @param title The string contents for this piece of data
     * @param likes The number of likes of this piece of data
     * @param dislikes The number of dislikes for this piece of data
     */
    Datum(int id, String title, int likes, int dislikes) {
        mId = id;
        mTitle = title;
        mLike = likes;
        mDislike = dislikes;
    }

    public int getmId() {
        return mId;
    }
}
