package maven_in_the_USA.cse216.lehigh.edu.android;

import android.content.Context;
<<<<<<< HEAD
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
=======
//import android.support.v7.widget.RecyclerView;
>>>>>>> android
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView msg;
        TextView mLike;
        TextView mDislike;
        Button bLike;
        Button bDislike;

        ViewHolder(View itemView) {
            super(itemView);
            this.msg = (TextView) itemView.findViewById(R.id.list_item_message);
            this.mLike = (TextView) itemView.findViewById(R.id.likeButton);
            this.mDislike = (TextView) itemView.findViewById(R.id.dislikeButton);
        }
    }

    private ArrayList<Entry> mData;
    private LayoutInflater mLayoutInflater;

    ItemListAdapter(Context context, ArrayList<Entry> data) {
        mData = data;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Entry d = mData.get(position);

        // set the content for the list item
        //holder.msg.setText("[" + Integer.toString(d.mId) + "] " + d.mTitle + ": " + d.mContent);
<<<<<<< HEAD
        // previous line is commented out bc backend doesnt have mContent so it gives error
        holder.msg.setText("[" + Integer.toString(d.mId) + "] " + d.mTitle);
        holder.mLike.setText(Integer.toString(d.mLike));
        holder.mDislike.setText(Integer.toString(d.mDislike));


        //Definitely something wrong here and im sorry i tried to fix it by idk what to do inside "try" - same for dislike
        /*Button bLike = (Button) findViewById(R.id.likeButton);
        bLike.setOnClickListener(new View.OnClickListener() {
                                     String uri = "https://mysterious-mountain-99747.herokuapp.com/messages/";
                                     String id_uri = uri.concat(String.valueOf(d.mId)).concat("upvotes");

            JsonObjectRequest jsonR = new JsonObjectRequest(Request.Method.PUT, id_uri, null, new Response.Listener<JSONObject>() {
                                         @Override
                                         public void onResponse(JSONObject response) {
                                             try {
                                                 String mStatus = response.getString("mStatus");
                                                 int mLike = response.getInt("mLike");
                                                 getLikes(mLike);
                                             } catch (JSONException e) {
                                                 e.printStackTrace();
                                             }
                                         }
                                     }, new Response.ErrorListener() {
                                         @Override
                                         public void onErrorResponse(VolleyError error) {
                                             Log.e("updebug", "Like message didn't work");
                                         }
                                     }
                                     );
                                 });
        Button bDislike = (Button) findViewById(R.id.likeButton);
        bDislike.setOnClickListener(new View.OnClickListener() {
            String uri = "https://mysterious-mountain-99747.herokuapp.com/messages/";
            String id_uri = uri.concat(String.valueOf(d.mId)).concat("downvotes");

            JsonObjectRequest jsonR = new JsonObjectRequest(Request.Method.PUT, id_uri, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String mStatus = response.getString("mStatus");
                        int mLike = response.getInt("mLike");
                        getDislikes(mLike);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("updebug", "Like message didn't work");
                }
            }
            );
        });*/
=======
        holder.msg.setText("[" + Integer.toString(d.mId) + "] " + d.mTitle + ": ");

>>>>>>> android

        // Attach a click listener to the view we are configuring
        final View.OnClickListener listener = new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                mClickListener.onClick(d);
            }
        };
        holder.msg.setOnClickListener(listener);
    }

    interface ClickListener{
        void onClick(Entry d);
    }

    private ClickListener mClickListener;
    ClickListener getClickListener() {
        return mClickListener;
    }

    void setClickListener(ClickListener c) {
        mClickListener = c;
    }
}
