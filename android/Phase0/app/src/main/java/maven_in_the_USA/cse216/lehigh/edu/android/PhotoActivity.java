package maven_in_the_USA.cse216.lehigh.edu.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;

import android.hardware.camera2.*;
import android.hardware.camera2.params.*;
import android.graphics.*;
import android.provider.*;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ImageView;

//import volley dependencies individually
import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Request;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PhotoActivity extends AppCompatActivity {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    String title = null;
    String message = null;
    String newTitle = null;
    String newMessage = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bTakePic = (Button) findViewById(R.id.imageButton);
        Button bCancel = (Button) findViewById(R.id.cancelButton);
        final EditText etTitle = (EditText) findViewById(R.id.title_text);
        final EditText etMessage = (EditText) findViewById(R.id.message_text);

        bTakePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title = etTitle.getText().toString();
                message = etMessage.getText().toString();
                if(title == null) {
                    Toast.makeText(PhotoActivity.this, "Title is required", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(message == null) {
                    Toast.makeText(PhotoActivity.this, "Password is Required", Toast.LENGTH_SHORT).show();
                    return;
                }
                newTitle = title;
                newMessage = message;
                accessCamera();
            }
        });

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    // Create intent to access phone's Camera 
    private void accessCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    // Retreives and sends image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            String photo = Base64.encodeToString(byteArray, Base64.DEFAULT);
            sendPhoto(photo);
        }
    }

    /* send photo to backend? Or post to buzz?
    protected void sendPhoto(String photo) {

    }
    */
    
}